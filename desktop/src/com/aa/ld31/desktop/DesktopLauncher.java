package com.aa.ld31.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.aa.ld31.LD31Game;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;

import java.util.logging.Level;
import java.util.logging.Logger;

public class DesktopLauncher {
	public static void main (String[] arg) {
		boolean developerMode = true;
		if(developerMode){
			runTexturePacker();
		}

		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.title = "LD31 - Hyper Soccer";
		config.width = 1280;
		config.height = 720;

		new LwjglApplication(new LD31Game(), config);
	}

	private static void runTexturePacker(){
		try {
			TexturePacker.Settings settings = new TexturePacker.Settings();
			settings.filterMag = Texture.TextureFilter.Nearest;
			settings.filterMin = Texture.TextureFilter.Nearest;
			TexturePacker.process(settings, "gfx/gui/elements", "gfx/gui/", "myskin");
			TexturePacker.process(settings, "gfx/objects", "gfx/", "texture_atlas");
		}catch (Exception ex){
			Logger.getLogger(DesktopLauncher.class.getName()).log(Level.WARNING, "An exception occurred packing textures:", ex);
		}
	}
}
