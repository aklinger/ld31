package com.aa.ld31.gl;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

/**
 * Created by Toni on 06.12.2014.
 */
public class SmoothFont {
    private static Texture texture;
    private static BitmapFont smoothFont_large_b;
    private static BitmapFont smoothFont_small_b;
    private static BitmapFont smoothFont_large_w;
    private static BitmapFont smoothFont_small_w;
    private static ShaderProgram fontShader;

    public static void init(){
        texture = new Texture(Gdx.files.internal("gfx/fonts/testfont.png"), true);
        texture.setFilter(Texture.TextureFilter.MipMapLinearNearest, Texture.TextureFilter.Linear);
        smoothFont_large_b = new BitmapFont(Gdx.files.internal("gfx/fonts/testfont.fnt"), new TextureRegion(texture), false);
        smoothFont_large_b.setScale(1.5f);
        smoothFont_large_b.setColor(Color.BLACK);

        smoothFont_small_b = new BitmapFont(Gdx.files.internal("gfx/fonts/testfont.fnt"), new TextureRegion(texture), false);
        smoothFont_small_b.setScale(0.7f);
        smoothFont_small_b.setColor(Color.BLACK);

        smoothFont_large_w = new BitmapFont(Gdx.files.internal("gfx/fonts/testfont.fnt"), new TextureRegion(texture), false);
        smoothFont_large_w.setScale(1.5f);
        smoothFont_large_w.setColor(Color.WHITE);

        smoothFont_small_w = new BitmapFont(Gdx.files.internal("gfx/fonts/testfont.fnt"), new TextureRegion(texture), false);
        smoothFont_small_w.setScale(0.7f);
        smoothFont_small_w.setColor(Color.WHITE);

        fontShader = new ShaderProgram(Gdx.files.internal("gfx/shaders/font.vert"), Gdx.files.internal("gfx/shaders/font.frag"));
        if (!fontShader.isCompiled()) {
            Gdx.app.error("fontShader", "compilation failed:\n" + fontShader.getLog());
        }
    }

    public static void renderSmooth(BitmapFont smoothFont, SpriteBatch batch, float x, float y, String text){
        batch.setShader(fontShader);
        smoothFont.drawMultiLine(batch, text, x, y);
        batch.setShader(null);
    }
    public static void renderSmoothCentered(BitmapFont smoothFont, SpriteBatch batch, float x, float y, String text){
        BitmapFont.TextBounds textBounds = smoothFont.getMultiLineBounds(text);
        batch.setShader(fontShader);
        smoothFont.drawMultiLine(batch, text, x-textBounds.width/2, y-textBounds.height/2);
        batch.setShader(null);
    }


    public static void addFontToSkin(Skin skin){
        skin.add("smoothFont_large", smoothFont_large_b, BitmapFont.class);
        skin.add("smoothFont_small", smoothFont_small_b, BitmapFont.class);
    }

    public static ShaderProgram getFontShader() {
        return fontShader;
    }


    public static BitmapFont getSmoothFont_large_black() {
        return smoothFont_large_b;
    }

    public static BitmapFont getSmoothFont_large_white() {
        return smoothFont_large_w;
    }

    public static BitmapFont getSmoothFont_small_black() {
        return smoothFont_small_b;
    }

    public static BitmapFont getSmoothFont_small_white() {
        return smoothFont_small_w;
    }
}
