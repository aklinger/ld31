package com.aa.ld31.dal;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Loads and stores all the Textures.
 *
 * @author Toni
 */
public class TextureManager implements ResourceProvider {
    //Attributes
    private Map<String, String> nameMap;
    private TextureAtlas textureAtlas;
    private static TextureManager instance;

    //Constructors
    private TextureManager() {
        nameMap = new HashMap<>();
    }

    public static TextureManager getInstance() {
        if (instance == null) {
            instance = new TextureManager();
        }
        return instance;
    }

    //Methods
    public void loadTextures(String file){
        textureAtlas = new TextureAtlas(Gdx.files.internal(file));
    }
    public void loadTextureMetaData(String file) throws IOException{
        FileHandle fileHandle = Gdx.files.internal(file);
        BufferedReader reader = fileHandle.reader(256);
        String line;
        while ((line = reader.readLine()) != null) {
            if (!line.isEmpty() && !line.startsWith("#")) {
                String[] split = line.split(":");
                if(split.length >= 2) {
                    nameMap.put(split[1], split[0]);
                }
            }
        }
    }

    @Override
    public TextureRegion getTexture(String name) {
        if(textureAtlas != null) {
            String alt = nameMap.get(name);
            if(alt != null){
                return textureAtlas.findRegion(alt);
            }else{
                return textureAtlas.findRegion(name);
            }
        }else{
            return null;
        }
    }

    public void disposeAllTextures() {
        textureAtlas.dispose();
    }
}
