package com.aa.ld31.dal.input.mappings;

/**
 * Created by Toni on 07.12.2014.
 */
public abstract class SliderState implements InputState{
    private float valueLastFrame;
    private float valueCurrentFrame;
    private boolean invert;
    private static final float treshold = 0.5f;
    private static final float deadzone = 0.2f;

    public SliderState(boolean invert){
        this.invert = invert;
    }

    public void actFrame(){
        valueLastFrame = valueCurrentFrame;
    }

    public boolean isDown() {
        return valueCurrentFrame > treshold;
    }

    private boolean wasDownLastFrame(){
        return (valueLastFrame) > treshold;
    }

    public boolean isPressed() {
        return isDown() && !(wasDownLastFrame());
    }

    public boolean isReleased() {
        return !isDown() && wasDownLastFrame();
    }

    @Override
    public float getSliderValue() {
        return valueCurrentFrame;
    }

    @Override
    public float getSliderVelocity() {
        return valueCurrentFrame-valueLastFrame;
    }

    public void setValue(float value){
        if(invert){
            value *= -1;
        }
        if(value < deadzone){
            value = 0;
        }
        this.valueCurrentFrame = value;
    }

    public boolean isInvert() {
        return invert;
    }

    //Abstract Stuff
    public void updateWithAxisValue(int axis, float value){}
    public void updateWithKeyValue(int key, boolean pressed){}
    public void updateWithButtonValue(int button, boolean pressed){}
}
