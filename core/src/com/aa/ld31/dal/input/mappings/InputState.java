package com.aa.ld31.dal.input.mappings;

/**
 * Created by Toni on 07.12.2014.
 */
public interface InputState {
    void actFrame();
    boolean isDown();
    boolean isPressed();
    boolean isReleased();
    float getSliderValue();
    float getSliderVelocity();

    void updateWithAxisValue(int axis, float value);
    void updateWithKeyValue(int key, boolean pressed);
    void updateWithButtonValue(int button, boolean pressed);

    String getName();
}
