package com.aa.ld31.dal.input.mappings;

import java.util.Arrays;

/**
 * Created by Toni on 07.12.2014.
 */
public class MultiAxisMapping extends SliderState{
    private int[] axes;
    private float[] values;

    public MultiAxisMapping(boolean invert, int... axes) {
        super(invert);
        this.axes = axes;
        values = new float[axes.length];
    }

    @Override
    public void updateWithAxisValue(int axis, float value) {
        float maxValue = 0;
        boolean update = false;
        for (int i = 0; i < axes.length; i++) {
            int axe = axes[i];
            if (axe == axis) {
                values[i] = value;
                update = true;
            }
            if(values[i] > maxValue ^ isInvert()){
                maxValue = values[i];
            }
        }
        if(update) {
            setValue(maxValue);
        }
    }

    @Override
    public String getName() {
        return "Axis: "+ Arrays.toString(axes);
    }
}
