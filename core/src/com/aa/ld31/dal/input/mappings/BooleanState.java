package com.aa.ld31.dal.input.mappings;

/**
 * Created by Toni on 12.07.2014.
 */
public abstract class BooleanState implements InputState{
    private boolean pressedLastFrame;
    private boolean pressedCurrentFrame;

    public void press(){
        pressedCurrentFrame = true;
    }

    public void release(){
        pressedCurrentFrame = false;
    }

    public void actFrame(){
        pressedLastFrame = pressedCurrentFrame;
    }

    public boolean isDown() {
        return pressedCurrentFrame;
    }

    public boolean isPressed() {
        return pressedCurrentFrame && !pressedLastFrame;
    }

    public boolean isReleased() {
        return !pressedCurrentFrame && pressedLastFrame;
    }

    @Override
    public float getSliderValue() {
        return pressedCurrentFrame ? 1 : 0;
    }

    @Override
    public float getSliderVelocity() {
        return pressedLastFrame == pressedCurrentFrame ? 0 : (pressedCurrentFrame ? 1 : -1);
    }

    //Abstract Stuff
    public void updateWithAxisValue(int axis, float value){}
    public void updateWithKeyValue(int key, boolean pressed){}
    public void updateWithButtonValue(int button, boolean pressed){}
}
