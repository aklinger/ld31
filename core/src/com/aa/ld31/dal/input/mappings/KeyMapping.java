package com.aa.ld31.dal.input.mappings;

import com.badlogic.gdx.Input;

/**
 * Created by Toni on 12.07.2014.
 */
public class KeyMapping extends BooleanState {
    private int key;

    public KeyMapping(int key) {
        this.key = key;
    }

    public int getKey() {
        return key;
    }

    @Override
    public void updateWithKeyValue(int key, boolean pressed) {
        if(this.key == key) {
            if (pressed) {
                press();
            } else {
                release();
            }
        }
    }

    @Override
    public String getName() {
        return Input.Keys.toString(key);
    }
}
