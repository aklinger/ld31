package com.aa.ld31.dal.input.mappings;

/**
 * Created by Toni on 07.12.2014.
 */
public class ButtonMapping extends BooleanState {
    private int button;

    public ButtonMapping(int button) {
        this.button = button;
    }

    public int getButton() {
        return button;
    }

    @Override
    public void updateWithButtonValue(int button, boolean pressed) {
        if(this.button == button) {
            if (pressed) {
                press();
            } else {
                release();
            }
        }
    }

    @Override
    public String getName() {
        return "Button: "+button;
    }
}
