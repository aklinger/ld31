package com.aa.ld31.dal.input;

import com.aa.ld31.dal.input.mappings.InputState;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.graphics.Color;

import java.util.Map;

/**
 * Created by Toni on 07.12.2014.
 */
public class PlayerInputController implements Comparable<PlayerInputController> {

    private Map<InputAction, InputState> keyMap;
    private Color color;
    private transient Color brighterColor;
    private String name;
    private int index;
    private String textureName;
    private boolean autoAim = false;

    private Controller assignedController;

    public PlayerInputController(String name, int index, Color color, String textureName, Map<InputAction, InputState> keyMap) {
        this.keyMap = keyMap;
        this.name = name;
        this.index = index;
        this.color = color;
        this.brighterColor = color.cpy().lerp(Color.WHITE,0.5f);
        this.textureName = textureName;
    }

    public boolean isDown(InputAction key){
        return keyMap.get(key).isDown();
    }
    public boolean isPressed(InputAction key){
        return keyMap.get(key).isPressed();
    }
    public boolean isReleased(InputAction key){
        return keyMap.get(key).isReleased();
    }
    public float getSliderValue(InputAction action){
        return keyMap.get(action).getSliderValue();
    }

    public void actFrame() {
        for (InputState keyMapping : keyMap.values()) {
            keyMapping.actFrame();
        }
    }

    /*public void setKey(InputAction action, int keyCode){
        keyMap.put(action, new KeyMapping(keyCode));
    }
    public int getKeyCode(InputAction action){
        return keyMap.get(action).getKey();
    }*/

    public String getKeyName(InputAction action){
        if(isAssigned(action)) {
            return keyMap.get(action).getName();
        }else{
            throw new RuntimeException("Action '"+action.getLabel()+"' is not assigned to a key.");
        }
    }

    public boolean isAssigned(InputAction action){
        return keyMap.get(action) != null;
    }

    /*public void press(InputAction action) {
        keyMap.get(action).press();
    }
    public void release(InputAction action) {
        keyMap.get(action).release();
    }*/

    public boolean keyDown(int keycode) {
        for (InputState km : keyMap.values()) {
            km.updateWithKeyValue(keycode, true);
        }
        return false;
    }

    public boolean keyUp(int keycode) {
        for (InputState km : keyMap.values()) {
            km.updateWithKeyValue(keycode, false);
        }
        return false;
    }

    public boolean buttonDown(Controller controller, int buttonCode) {
        if(controller == assignedController){
            for (InputState km : keyMap.values()) {
                km.updateWithButtonValue(buttonCode, true);
            }
        }
        return false;
    }

    public boolean buttonUp(Controller controller, int buttonCode) {
        if(controller == assignedController){
            for (InputState km : keyMap.values()) {
                km.updateWithButtonValue(buttonCode, false);
            }
        }
        return false;
    }

    public boolean axisMoved(Controller controller, int axisCode, float value) {
        if(controller == assignedController){
            for (InputState km : keyMap.values()) {
                km.updateWithAxisValue(axisCode, value);
            }
        }
        return false;
    }

    public boolean povMoved(Controller controller, int povCode, PovDirection value) {
        return false;
    }

    public void setAssignedController(Controller assignedController) {
        this.assignedController = assignedController;
    }

    public Controller getAssignedController() {
        return assignedController;
    }

    public String getName() {
        return name;
    }

    public String getTextureName() {
        return textureName;
    }

    public boolean isAutoAim() {
        return autoAim;
    }

    public void setAutoAim(boolean autoAim) {
        this.autoAim = autoAim;
    }

    @Override
    public int compareTo(PlayerInputController playerInputController) {
        return this.getName().compareTo(playerInputController.getName());
    }

    public Color getColor() {
        return color;
    }

    public Color getBrighterColor(){
        return brighterColor;
    }

    public int getIndex() {
        return index;
    }
}
