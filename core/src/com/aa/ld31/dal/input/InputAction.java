package com.aa.ld31.dal.input;

/**
 * Created by Toni on 12.07.2014.
 */
public enum InputAction {
    ACTION_MOVE_UP("action.move.up","Move Up"), ACTION_MOVE_DOWN("action.move.down","Move Down"),
    ACTION_MOVE_LEFT("action.move.left","Move Left"), ACTION_MOVE_RIGHT("action.move.right","Move Right"),
    ACTION_SHOOT("action.shoot", "Shoot"), ACTION_SPECIAL("action.special", "Special"),
    ACTION_SWITCH("action.switch", "Switch Player"),
    RESTART("meta.restart","Restart Level"), PAUSE("meta.pause", "Pause");

    private String code;
    private String label;

    private InputAction(String code, String label){
        this.code = code;
        this.label = label;
    }

    public String getCode() {
        return code;
    }

    public String getLabel() {
        return label;
    }
}
