package com.aa.ld31.dal.input;

import com.aa.ld31.dal.input.mappings.MultiAxisMapping;
import com.aa.ld31.dal.input.mappings.ButtonMapping;
import com.aa.ld31.dal.input.mappings.InputState;
import com.aa.ld31.dal.input.mappings.KeyMapping;
import com.aa.ld31.maths.ColorUtil;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Toni on 12.07.2014.
 */
public class ControlSettings implements InputProcessor, ControllerListener {

    private List<PlayerInputController> playerInputControllers;

    private static final ControlSettings instance;

    private static final Color[] PLAYER_COLORS = {new Color(0.95f,0.1f,0.1f,1), new Color(0.1f,0.1f,0.95f,1), new Color(0.1f, 0.95f, 0.1f, 1), new Color(0.95f, 0.95f, 0.1f, 1), new Color(0.88f, 0.1f, 0.88f, 1), Color.TEAL};

    static{
        instance = new ControlSettings();
    }

    public static ControlSettings getInstance(){
        return instance;
    }

    private ControlSettings(){
        playerInputControllers = new ArrayList<>(2);

        HashMap<InputAction, InputState> keyMap = new HashMap<>();
        keyMap.put(InputAction.ACTION_MOVE_LEFT, new KeyMapping(Input.Keys.A));
        keyMap.put(InputAction.ACTION_MOVE_RIGHT, new KeyMapping(Input.Keys.D));
        keyMap.put(InputAction.ACTION_MOVE_UP, new KeyMapping(Input.Keys.W));
        keyMap.put(InputAction.ACTION_MOVE_DOWN, new KeyMapping(Input.Keys.S));

        keyMap.put(InputAction.ACTION_SPECIAL, new KeyMapping(Input.Keys.J));
        keyMap.put(InputAction.ACTION_SWITCH, new KeyMapping(Input.Keys.K));
        keyMap.put(InputAction.ACTION_SHOOT, new KeyMapping(Input.Keys.L));

        keyMap.put(InputAction.RESTART, new KeyMapping(Input.Keys.R));
        keyMap.put(InputAction.PAUSE, new KeyMapping(Input.Keys.P));

        PlayerInputController player1 = new PlayerInputController("P1", 0, getPlayerColor(0), "controls0", keyMap);
        playerInputControllers.add(player1);

        keyMap = new HashMap<>();
        keyMap.put(InputAction.ACTION_MOVE_LEFT, new KeyMapping(Input.Keys.LEFT));
        keyMap.put(InputAction.ACTION_MOVE_RIGHT, new KeyMapping(Input.Keys.RIGHT));
        keyMap.put(InputAction.ACTION_MOVE_UP, new KeyMapping(Input.Keys.UP));
        keyMap.put(InputAction.ACTION_MOVE_DOWN, new KeyMapping(Input.Keys.DOWN));

        keyMap.put(InputAction.ACTION_SPECIAL, new KeyMapping(Input.Keys.NUMPAD_1));
        keyMap.put(InputAction.ACTION_SWITCH, new KeyMapping(Input.Keys.NUMPAD_2));
        keyMap.put(InputAction.ACTION_SHOOT, new KeyMapping(Input.Keys.NUMPAD_3));

        keyMap.put(InputAction.RESTART, new KeyMapping(Input.Keys.R));
        keyMap.put(InputAction.PAUSE, new KeyMapping(Input.Keys.P));

        PlayerInputController player2 = new PlayerInputController("P2", 1, getPlayerColor(1), "controls1", keyMap);
        playerInputControllers.add(player2);

        Array<Controller> controllers = Controllers.getControllers();
        int player = 3;
        for(Controller controller : controllers){
            System.out.println("Controller detected = " + controller.getName());

            keyMap = new HashMap<>();
            keyMap.put(InputAction.ACTION_MOVE_LEFT, new MultiAxisMapping(true, 1, 3));
            keyMap.put(InputAction.ACTION_MOVE_RIGHT, new MultiAxisMapping(false, 1, 3));
            keyMap.put(InputAction.ACTION_MOVE_UP, new MultiAxisMapping(true, 0, 2));
            keyMap.put(InputAction.ACTION_MOVE_DOWN, new MultiAxisMapping(false, 0, 2));

            keyMap.put(InputAction.ACTION_SPECIAL, new ButtonMapping(1));
            keyMap.put(InputAction.ACTION_SWITCH, new ButtonMapping(5));
            keyMap.put(InputAction.ACTION_SHOOT, new ButtonMapping(3));

            keyMap.put(InputAction.RESTART, new ButtonMapping(10));
            keyMap.put(InputAction.PAUSE, new ButtonMapping(9));

            PlayerInputController additionalPlayers = new PlayerInputController("P"+player, player-1, getPlayerColor(player-1), "controls2", keyMap);
            additionalPlayers.setAssignedController(controller);
            playerInputControllers.add(additionalPlayers);

            player++;
        }
    }

    public boolean isDown(InputAction key, int player){
        return playerInputControllers.get(player).isDown(key);
    }
    public boolean isPressed(InputAction key, int player){
        return playerInputControllers.get(player).isPressed(key);
    }
    public boolean isReleased(InputAction key, int player){
        return playerInputControllers.get(player).isReleased(key);
    }

    public void actFrame(){
        for(PlayerInputController playerInputController : playerInputControllers) {
            playerInputController.actFrame();
        }
    }

    /*public void setKey(InputAction action, int keyCode, int player){
        playerInputControllers.get(player).setKey(action, keyCode);
    }
    public int getKeyCode(InputAction action, int player){
        return playerInputControllers.get(player).getKeyCode(action);
    }*/

    public String getKeyName(InputAction action, int player){
        return playerInputControllers.get(player).getKeyName(action);
    }

    public boolean isAssigned(InputAction action, int player){
        return playerInputControllers.get(player).isAssigned(action);
    }

    /*public void press(InputAction action, int player) {
        playerInputControllers.get(player).press(action);
    }
    public void release(InputAction action, int player) {
        playerInputControllers.get(player).release(action);
    }*/

    @Override
    public boolean keyDown(int keycode) {
        System.out.println("keycode = " + keycode);
        for(PlayerInputController playerInputController : playerInputControllers) {
            playerInputController.keyDown(keycode);
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        for(PlayerInputController playerInputController : playerInputControllers) {
            playerInputController.keyUp(keycode);
        }
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public void connected(Controller controller) {
        System.out.println("Connected: controller = " + controller.getName());
    }

    @Override
    public void disconnected(Controller controller) {
        System.out.println("Disconnected: controller = " + controller.getName());
    }

    @Override
    public boolean buttonDown(Controller controller, int buttonCode) {
        for(PlayerInputController playerInputController : playerInputControllers) {
            playerInputController.buttonDown(controller, buttonCode);
        }
        return false;
    }

    @Override
    public boolean buttonUp(Controller controller, int buttonCode) {
        for(PlayerInputController playerInputController : playerInputControllers) {
            playerInputController.buttonUp(controller, buttonCode);
        }
        return false;
    }

    @Override
    public boolean axisMoved(Controller controller, int axisCode, float value) {
        for(PlayerInputController playerInputController : playerInputControllers) {
            playerInputController.axisMoved(controller, axisCode, value);
        }
        return false;
    }

    @Override
    public boolean povMoved(Controller controller, int povCode, PovDirection value) {
        return false;
    }

    @Override
    public boolean xSliderMoved(Controller controller, int sliderCode, boolean value) {
        return false;
    }

    @Override
    public boolean ySliderMoved(Controller controller, int sliderCode, boolean value) {
        return false;
    }

    @Override
    public boolean accelerometerMoved(Controller controller, int accelerometerCode, Vector3 value) {
        return false;
    }

    private static Color getPlayerColor(int index){
        if(index < PLAYER_COLORS.length){
            return PLAYER_COLORS[index];
        }else{
            return ColorUtil.randomColor(1);
        }
    }

    public List<PlayerInputController> getPlayerInputControllers() {
        return playerInputControllers;
    }
}
