package com.aa.ld31.dal.audio;

/**
 * Created by Toni on 28.04.2014.
 */
public class SoundFX {
    //Music
    public static final String MUSIC_MAIN_MENU = "music/menu_music_3.mp3";

    //Sound Effects
    public static final String KICK1 = "sfx/Kick.wav";
    public static final String KICK2 = "sfx/Kick2.wav";

    public static final String[] KICKS = {KICK1, KICK2};

    public static final String BUTTON = "sfx/Beep.wav";
    public static final String HOOKSHOT = "sfx/Boing.wav";
    public static final String HOOKSHOT_HIT = "sfx/Hookshot.wav";
    public static final String SHOCKWAVE = "sfx/Shockwave.wav";
    public static final String DASH = "sfx/Woosh.wav";
    public static final String GROW = "sfx/Blop.wav";

    public static final String FIREWORKS1 = "sfx/Fireworks1.wav";
    public static final String FIREWORKS2 = "sfx/Fireworks2.wav";
    public static final String FIREWORKS3 = "sfx/Fireworks3.wav";
    public static final String FIREWORKS4 = "sfx/Fireworks4.wav";
    public static final String FIREWORKS5 = "sfx/Fireworks5.wav";
    public static final String FIREWORKS6 = "sfx/Fireworks6.wav";

    public static final String[] FIREWORKS = {FIREWORKS1, FIREWORKS2, FIREWORKS3, FIREWORKS4, FIREWORKS5, FIREWORKS6};

    public static final String BUMP = "sfx/Bump.wav";
    public static final String BOING = "sfx/Boing.wav";


}
