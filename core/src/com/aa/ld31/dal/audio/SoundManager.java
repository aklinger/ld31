package com.aa.ld31.dal.audio;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;

import java.util.HashMap;
import java.util.Map;

/**
 * Manages a Cache of sound Effects.
 * <p/>
 * Created by Toni on 12.07.2014.
 */
public class SoundManager {
    //Attributes
    private float volume = 1f;
    private boolean enabled = true;
    private final Map<String, Sound> sounds;

    //Constructors
    private static SoundManager instance;

    public static SoundManager getInstance() {
        if (instance == null) {
            instance = new SoundManager();
        }
        return instance;
    }

    public SoundManager() {
        sounds = new HashMap<>();
    }

    //Methods
    public void playRandom(float volume, String... sounds) {
        int chosen = (int) (Math.random() * sounds.length);
        play(sounds[chosen], volume);
    }

    public void play(String sound){
        play(sound,1);
    }

    public void play(String sound, float volumeMultiplier) {
        if (!enabled) {
            return;
        }

        Sound soundToPlay = sounds.get(sound);
        if (soundToPlay == null) {
            soundToPlay = load(sound);
        }

        //MyLogger.debug("Playing sound: " + sound);

        soundToPlay.play(this.volume*volumeMultiplier);
    }

    public Sound load(String sound){
        FileHandle soundFile = Gdx.files.internal(sound);
        Sound soundToLoad = Gdx.audio.newSound(soundFile);
        sounds.put(sound, soundToLoad);
        return soundToLoad;
    }

    public void dispose() {
        //MyLogger.log("Disposing the sound manager");
        for (Sound sound : sounds.values()) {
            sound.stop();
            sound.dispose();
        }
        sounds.clear();
    }

    public void setVolume(float volume) {
        this.volume = volume;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public float getVolume() {
        return volume;
    }

    public boolean isEnabled() {
        return enabled;
    }
}
