package com.aa.ld31.dal.audio;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Disposable;

/**
 * A service that manages the background music.
 * <p/>
 * Only one music may be playing at a given time.
 * <p/>
 * Created by Toni on 12.07.2014.
 */
public class MusicManager implements Disposable {
    /**
     * Holds the music currently being played, if any.
     */
    private Music musicBeingPlayed;

    /**
     * Holds the music currently being played, if any.
     */
    private String musicBeingPlayed_name;

    /**
     * Holds the music requested to be played, if any.
     */
    private String musicToBePlayed_name;

    /**
     * The volume to be set on the music.
     */
    private float volume = 1f;

    /**
     * Whether the music is enabled.
     */
    private boolean enabled = true;

    /**
     * Creates the music manager.
     */
    private MusicManager() {
    }

    private static MusicManager instance;

    public static MusicManager getInstance() {
        if (instance == null) {
            instance = new MusicManager();
        }
        return instance;
    }

    /**
     * Plays the given music (starts the streaming).
     * <p/>
     * If there is already a music being played it is stopAndDisposed
     * automatically.
     */
    public void play(String music) {
        // check if the music is enabled
        if (enabled) {
            // stopAndDispose any music being played
            //MyLogger.log("Playing music: " + music);
            stopAndDispose();

            // start streaming the new music
            FileHandle musicFile = Gdx.files.internal(music);
            musicBeingPlayed = Gdx.audio.newMusic(musicFile);
            musicBeingPlayed.setVolume(volume);
            musicBeingPlayed.setLooping(true);
            musicBeingPlayed.play();
            musicBeingPlayed_name = music;
        }
        musicToBePlayed_name = music;
    }

    public void playIfNotAlreadyPlaying(String music_name) {
        if (enabled) {
            if (musicBeingPlayed_name == null || !musicBeingPlayed_name.equals(music_name)) {
                play(music_name);
            } else if (!musicBeingPlayed.isPlaying()) {
                musicBeingPlayed.stop();
                musicBeingPlayed.play();
            }
        }
        musicToBePlayed_name = music_name;
    }

    /**
     * Stops and disposes the current music being played, if any.
     */
    public void stopAndDispose() {
        if (musicBeingPlayed != null) {
            //MyLogger.log("Stopping current Music:");
            musicBeingPlayed.stop();
            musicBeingPlayed.dispose();
            musicBeingPlayed = null;
            musicBeingPlayed_name = null;
        }
        musicToBePlayed_name = null;
    }

    public void invalidate(){
        if (musicBeingPlayed != null) {
            //MyLogger.log("Invalidating current Music:");
            musicBeingPlayed.stop();
            musicBeingPlayed.dispose();
            musicBeingPlayed = null;
            musicBeingPlayed_name = null;
        }
    }

    public void pause() {
        if (musicBeingPlayed != null) {
            //MyLogger.log("Pausing current Music:");
            musicBeingPlayed.pause();
        }
    }

    public void resume() {
        if(musicToBePlayed_name != null && !musicToBePlayed_name.equals(musicBeingPlayed_name)){
            play(musicToBePlayed_name);
        }
        if (musicBeingPlayed != null && !musicBeingPlayed.isPlaying()) {
            //MyLogger.log("Resuming current Music:");
            musicBeingPlayed.play();
        }
    }

    /**
     * Sets the music volume which must be inside the range [0,1].
     */
    public void setVolume(float volume) {
        //MyLogger.log("Adjusting music volume to: " + volume);

        // check and set the new volume
        if (volume < 0 || volume > 1f) {
            throw new IllegalArgumentException("The volume must be inside the range: [0,1]");
        }
        this.volume = volume;

        // if there is a music being played, change its volume
        if (musicBeingPlayed != null) {
            musicBeingPlayed.setVolume(volume);
        }
    }

    /**
     * Enables or disabled the music.
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;

        // if the music is being deactivated, stopAndDispose any music being played
        if (!enabled) {
            pause();
        } else {
            resume();
        }
    }

    /**
     * Disposes the music manager.
     */
    @Override
    public void dispose() {
        //MyLogger.log("Disposing Music Manager");
        stopAndDispose();
    }

    public boolean isEnabled() {
        return enabled;
    }

    public float getVolume() {
        return volume;
    }
}
