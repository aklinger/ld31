package com.aa.ld31.dal;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * An abstract way of accessing Resources.
 * <p/>
 * Created by Toni on 12.07.2014.
 */
public interface ResourceProvider {
    //Methods
    public TextureRegion getTexture(String name);
}