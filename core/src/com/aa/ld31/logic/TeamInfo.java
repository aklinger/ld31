package com.aa.ld31.logic;

import com.aa.ld31.dal.input.InputAction;
import com.aa.ld31.dal.input.PlayerInputController;
import com.aa.ld31.logic.objects.Token;
import com.badlogic.gdx.math.Vector2;

import java.util.*;

/**
 * Created by Toni on 07.12.2014.
 */
public class TeamInfo {
    private Team team;
    private int score;
    private List<Token> tokens;
    private Map<PlayerInputController, Token> selections;
    private Map<PlayerInputController, TempPlayerData> tempData;

    public TeamInfo(Team team) {
        this.team = team;
        tokens = new ArrayList<>();
        selections = new HashMap<>();
        tempData = new HashMap<>();
    }

    public void handleInput(EntityManager manager){
        for(PlayerInputController playerInputController : selections.keySet()){
            Token selected = selections.get(playerInputController);
            checkInput(manager, playerInputController, selected);
        }
        for(TempPlayerData tempPlayerData : tempData.values()){
            tempPlayerData.act();
        }
    }

    private void checkInput(EntityManager manager, PlayerInputController playerInputController, Token token){
        float xAxis = playerInputController.getSliderValue(InputAction.ACTION_MOVE_RIGHT)-playerInputController.getSliderValue(InputAction.ACTION_MOVE_LEFT);
        float yAxis = playerInputController.getSliderValue(InputAction.ACTION_MOVE_UP)-playerInputController.getSliderValue(InputAction.ACTION_MOVE_DOWN);
        Vector2 direction = new Vector2(xAxis, yAxis);
        if(token != null) {
            token.moveInDirection(manager, direction);

            if (playerInputController.isPressed(InputAction.ACTION_SPECIAL)) {
                Vector2 specialUseDirection;
                if(playerInputController.isAutoAim()){
                    specialUseDirection = null;
                }else{
                    specialUseDirection = new Vector2(direction);
                }
                token.tryToUseSpecial(manager, specialUseDirection);
            }
        }

        if(playerInputController.isPressed(InputAction.ACTION_SWITCH)){
            switchToClosest(playerInputController, direction);
        }
    }

    private void switchToClosest(PlayerInputController playerInputController, Vector2 direction) {
        Token nextSelected = getClosestTokenInDirection(playerInputController, direction);
        if(nextSelected != null){
            selections.put(playerInputController, nextSelected);
            tempData.get(playerInputController).triggerTransition();
        }
    }

    public Token getClosestTokenInDirection(PlayerInputController playerInputController, Vector2 direction) {
        float minHeuristic = Float.MAX_VALUE;
        Token nextSelected = null;
        Token currentSelected = selections.get(playerInputController);
        for(Token otherToken : tokens){
            if(!getSelected().contains(otherToken)){
                if(currentSelected == null){
                    nextSelected = otherToken;
                    break;
                }
                if(otherToken != currentSelected){
                    Vector2 delta = otherToken.getCenter().sub(currentSelected.getCenter());
                    float dist = delta.len();
                    float heuristic = dist;
                    if(!direction.isZero()) {
                        float angle = direction.angle() - delta.angle();
                        if (angle > 180){
                            angle -= 360;
                        }else if (angle < -180) {
                            angle += 360;
                        }
                        angle = Math.abs(angle) * 6f;
                        heuristic = dist * dist + angle * angle;
                    }
                    if(heuristic < minHeuristic){
                        minHeuristic = heuristic;
                        nextSelected = otherToken;
                    }
                }
            }
        }
        return nextSelected;
    }

    public void addToTokens(Token token){
        tokens.add(token);
        for(PlayerInputController playerInputController : selections.keySet()){
            if(selections.get(playerInputController) == null){
                selections.put(playerInputController, token);
                break;
            }
        }
    }

    public void addPlayerInputController(PlayerInputController playerInputController){
        selections.put(playerInputController, null);
        tempData.put(playerInputController, new TempPlayerData(playerInputController));
    }

    public void addScore(int score) {
        this.score += score;
    }

    public Team getTeam() {
        return team;
    }

    public int getScore() {
        return score;
    }

    public List<Token> getTokens() {
        return tokens;
    }

    public Collection<Token> getSelected() {
        return selections.values();
    }

    public Map<PlayerInputController, Token> getSelections() {
        return selections;
    }

    public TempPlayerData getTempPlayerData(PlayerInputController playerInputController){
        return tempData.get(playerInputController);
    }

    class TempPlayerData{
        private PlayerInputController playerInputController;
        private float transitionDuration = 5;
        private float transitionTimer;
        private Vector2 selectionPos;
        private float selectionRot;

        private TempPlayerData(PlayerInputController playerInputController){
            this.playerInputController = playerInputController;
            selectionPos = new Vector2();
            selectionRot = playerInputController.getIndex()*5;
        }

        private void act() {
            selectionRot += 1;
            if (transitionTimer > 0){
                transitionTimer--;
                selectionPos.add(selections.get(playerInputController).getCenter()).scl(0.5f);
            }else{
                selectionPos.set(selections.get(playerInputController).getCenter());
            }
        }

        private void triggerTransition(){
            transitionTimer = transitionDuration;
        }

        public float getSelectionRot(){
            return selectionRot;
        }

        public Vector2 getSelectionPos(){
            if (transitionTimer > 0){
                return selectionPos;
            }else{
                return selections.get(playerInputController).getCenter();
            }
        }
    }
}
