package com.aa.ld31.logic;

import com.aa.ld31.dal.TextureManager;
import com.aa.ld31.gl.SmoothFont;
import com.aa.ld31.logic.objects.*;
import com.aa.ld31.logic.particle.AnimatedParticle;
import com.aa.ld31.maths.Calc;
import com.aa.ld31.maths.ColorUtil;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Toni on 16.04.2014.
 */
public class EntityFactory {

    public static TextureRegion loadSingleFrame(String name){
        return TextureManager.getInstance().getTexture(name);
    }

    public static TextureRegion[] loadAnimationFrames(String name, int length){
        TextureRegion[] texs = new TextureRegion[length];
        for (int i = 1; i <= length; i++) {
            texs[i-1] = TextureManager.getInstance().getTexture(name+i);
        }
        return texs;
    }

    public static TextureRegion[] loadAnimationFromSpriteSheet(String name, int start, int end, int cols, int rows){
        TextureRegion[] texs = new TextureRegion[end-start+1];
        TextureRegion textureRegion = TextureManager.getInstance().getTexture(name);
        TextureRegion[][] split = textureRegion.split(textureRegion.getRegionWidth() / cols, textureRegion.getRegionHeight() / rows);

        for (int i = 0; i <= end-start; i++) {
            texs[i] = split[(start+i)/cols][(start+i)%cols];
        }
        return texs;
    }

    public enum ObjectType {
        BALANCED, BALL, RUNNER, GRAPPLE, MAGNET, SHIELD;
    }

    public static ObjectType getRandomToken(){
        return ObjectType.values()[(int)(2+Math.random()*4)];
    }

    public static ObjectType getTokenByIndex(int index){
        return ObjectType.values()[(int)(2+index%4)];
    }

    public static Entity createEntity(ObjectType objectType, Team team){
        Fanta f;
        Animation anim;
        float tokenSize = 64;
        switch(objectType)
        {
            case BALL:
                f = new Fanta("still");

                anim = new Animation(50,loadSingleFrame("ball"));
                anim.setPlayMode(Animation.PlayMode.LOOP);
                f.putAnimation("still",anim);

                anim = new Animation(8,loadAnimationFromSpriteSheet("ball_sheet", 0, 22, 23, 1));
                anim.setPlayMode(Animation.PlayMode.LOOP);
                f.putAnimation("rolling",anim);

                anim = new Animation(4,loadAnimationFromSpriteSheet("ball_sheet", 0, 22, 23, 1));
                anim.setPlayMode(Animation.PlayMode.LOOP);
                f.putAnimation("fast",anim);

                Ball ball = new Ball(f);

                ball.setDrawableSize(32, 32, true);
                ball.setSolid(false);

                return ball;
            case MAGNET:
                f = new Fanta("default");

                anim = new Animation(50,loadSingleFrame("magnet"));
                anim.setPlayMode(Animation.PlayMode.LOOP);
                f.putAnimation("default",anim);

                MagnetToken magnet = new MagnetToken(f);

                magnet.setDrawableSize(tokenSize, tokenSize, true);
                magnet.setColor(team.getColor());
                magnet.setTeam(team);

                return magnet;
            case SHIELD:
                f = new Fanta("default");

                anim = new Animation(50,loadSingleFrame("shield"));
                anim.setPlayMode(Animation.PlayMode.LOOP);
                f.putAnimation("default", anim);

                ShieldToken shieldToken = new ShieldToken(f);

                shieldToken.setDrawableSize(tokenSize, tokenSize, true);
                shieldToken.setColor(team.getColor());
                shieldToken.setTeam(team);

                return shieldToken;
            case GRAPPLE:
                f = new Fanta("default");

                anim = new Animation(50,loadSingleFrame("grapple"));
                anim.setPlayMode(Animation.PlayMode.LOOP);
                f.putAnimation("default", anim);

                GrappleToken grapple = new GrappleToken(f);
                grapple.setColor(team.getColor());
                grapple.setTeam(team);

                grapple.setDrawableSize(tokenSize, tokenSize, true);

                return grapple;
            case RUNNER:
                f = new Fanta("default");

                anim = new Animation(50,loadSingleFrame("runner"));
                anim.setPlayMode(Animation.PlayMode.LOOP);
                f.putAnimation("default",anim);

                SpeedToken runner = new SpeedToken(f);

                runner.setDrawableSize(tokenSize, tokenSize, true);
                runner.setColor(team.getColor());
                runner.setTeam(team);

                return runner;
            default:
                throw new RuntimeException("ObjectType not supported: "+objectType);
        }
    }

    public static void createParticleBurst(Vector2 position, float displacement, int amount, float duration, Vector2 direction, float spreadDegree, float speedMin, float speedMax, float friction, Color color, int size, EntityManager manager){
        Fanta f = new Fanta("stuff");
        Animation anim = new Animation(0, new TextureRegion(TextureManager.getInstance().getTexture("particle")));
        f.putAnimation("stuff",anim);

        AnimatedParticle particle;
        for (int i = 0; i < amount; i++) {
            particle = new AnimatedParticle(f);
            particle.setDrawableSize(size, size, false);
            particle.setDuration(duration + (float) (Math.random() * 10));
            particle.setX(position.x + (float) (Math.random() * displacement) - displacement / 2 - size / 2);
            particle.setY(position.y + (float) (Math.random() * displacement) - displacement / 2 - size / 2);
            particle.setFx(friction);
            particle.setFy(friction);
            float flySpeed = (float)Calc.randomValueInRange(speedMin,speedMax);
            float directionRadians = (float)(Math.atan2(direction.x, direction.y)+(Math.random()*Math.toRadians(spreadDegree))-Math.toRadians(spreadDegree)/2);
            particle.setSx((float) (Math.sin(directionRadians)) * flySpeed);
            particle.setSy((float) (Math.cos(directionRadians)) * flySpeed);
            particle.setColor(color);
            manager.addFancyStuff(particle);
        }
    }

    public static void createFireworks(float x, float y, Color color, EntityManager manager) {
        Fanta f = new Fanta("stuff");
        Animation anim = new Animation(0, new TextureRegion(TextureManager.getInstance().getTexture("particle")));
        f.putAnimation("stuff",anim);

        float size = 16;
        float duration = 70;
        float displacement = 8;
        float friction = 0.8f;
        float gravity = -0.8f;
        float speedMin = 25;
        float speedMax = 55;
        float spreadDegree = 360;

        AnimatedParticle particle;
        for (int i = 0; i < 35; i++) {
            particle = new AnimatedParticle(f);
            particle.setDrawableSize(size, size, false);
            particle.setDuration(duration + (float) (Math.random() * 30));
            particle.setX(x + (float) (Math.random() * displacement) - displacement / 2 - size / 2);
            particle.setY(y + (float) (Math.random() * displacement) - displacement / 2 - size / 2);
            particle.setFx(friction);
            particle.setFy(friction);
            particle.setGy(gravity);
            float flySpeed = (float)Calc.randomValueInRange(speedMin,speedMax);
            float directionRadians = (float)((Math.random()*Math.toRadians(spreadDegree))-Math.toRadians(spreadDegree)/2);
            particle.setSx((float) (Math.sin(directionRadians)) * flySpeed);
            particle.setSy((float) (Math.cos(directionRadians)) * flySpeed);
            particle.setColor(ColorUtil.jumbleColor(color, 0.5f));
            particle.setLazor(true);
            manager.addFancyStuff(particle);
        }
    }

    public static void createTrail(Vector2 position, EntityManager entityManager){
        Fanta f = new Fanta("stuff");
        Animation anim = new Animation(0, new TextureRegion(TextureManager.getInstance().getTexture("runner")));
        f.putAnimation("stuff",anim);

        AnimatedParticle particle;
        particle = new AnimatedParticle(f);
        float size = 64;
        particle.setDrawableSize(size, size, false);
        particle.setDuration(5 + (float) (Math.random() * 5));
        particle.setX(position.x - size/2);
        particle.setY(position.y - size/2);
        particle.setColor(ColorUtil.randomColor(0.5f));
        entityManager.addFancyStuff(particle);
    }

    public static void createShockWave(Vector2 position, EntityManager entityManager){
        Fanta f = new Fanta("stuff");
        Animation anim = new Animation(0, new TextureRegion(TextureManager.getInstance().getTexture("shockwave")));
        f.putAnimation("stuff",anim);

        AnimatedParticle particle;
        particle = new AnimatedParticle(f);
        float size = 10;
        particle.setDrawableSize(size, size, false);
        particle.setDuration(7);
        particle.setX(position.x - size/2);
        particle.setY(position.y - size/2);
        particle.setColor(ColorUtil.randomColor(0.5f));

        particle.setFadeOut(true);
        particle.setGrowMaxSize(350*2);

        entityManager.addFancyStuff(particle);
    }

    public static void createTextParticle(Vector2 position, Color color, String text, EntityManager entityManager){
        AnimatedParticle particle;
        particle = new AnimatedParticle(text, SmoothFont.getSmoothFont_small_white());

        particle.setDuration(30);
        particle.setX(position.x);
        particle.setY(position.y);
        particle.setSy(0.5f);
        particle.setColor(ColorUtil.jumbleColor(color, 0.3f));

        particle.setFadeOut(true);

        entityManager.addFancyStuff(particle);
    }
}
