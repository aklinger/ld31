package com.aa.ld31.logic;

import com.aa.ld31.logic.objects.GrapplingHook;
import com.aa.ld31.maths.Collision;
import com.aa.ld31.maths.HitBox;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Toni on 15.04.2014.
 */
public abstract class Entity implements HitBox {
    //General Movement Attributes
    private float x;
    private float y;
    private float sx;
    private float sy;
    private float gx;
    private float gy;
    private float fx;
    private float fy;
    //Render Attributes
    private Fanta fanta;
    private float scale = 1;
    private Color color;
    private float imgW;
    private float imgH;
    private float rotation;
    private boolean flipX;
    private boolean flipY;
    //Collision Attributes
    private Rectangle hitBox;
    private boolean solid;
    private boolean fixated;
    private float mass;
    private float bounciness;
    public int spawnProtection;
    //Health Attributes
    private float maxHealth;
    private float health;

    private boolean hostile;
    private boolean draggable;


    //Constructor
    public Entity() {
        fx = 1;
        fy = 1;
        solid = true;
        mass = 1;
        bounciness = 0.5f;
        maxHealth = health = 10;
        spawnProtection = 1;
        color = Color.WHITE;
    }

    //Methoden
    public void computeMovement() {
        sx += gx;
        sy += gy;
        sx *= fx;
        sy *= fy;
        if(fixated){
            sx = 0;
            sy = 0;
        }
        x += sx;
        y += sy;
    }

    public abstract void act(EntityManager man);

    public boolean collidesWith(Entity other){
        if(this.isSolid() && other.isSolid()){
            return true;
        }
        return false;
    }

    public void interactWith(Entity ent2, Collision col, EntityManager man){
        //This should be abstract but I don't want to override this in EVERY subclass...
    }

    public void afterAct(EntityManager man){
        if(spawnProtection > 0) {
            spawnProtection--;
        }
    }

    public void render(SpriteBatch batch){
        batch.setColor(color);
        float originX = imgW/2;
        float originY = imgH/2;
        TextureRegion currentFrame = fanta.getCurrentFrame();
        batch.draw(currentFrame.getTexture(), getX(), getY(), originX, originY, imgW, imgH, scale, scale, rotation, currentFrame.getRegionX(), currentFrame.getRegionY(), currentFrame.getRegionWidth(), currentFrame.getRegionHeight(), flipX, flipY);
        batch.setColor(Color.WHITE);
    }
    public void debugRender(ShapeRenderer renderer){
        renderer.begin(ShapeRenderer.ShapeType.Line);
        Rectangle hitBox1 = getBoundingBox();
        renderer.rect(hitBox1.x,hitBox1.y,hitBox1.width,hitBox1.height);
        renderer.end();
    }

    public void die(EntityManager man){
        //This happens after it dies.
    }

    //Health Methods
    public void setMaxHealth(float maxHealth){
        this.maxHealth = maxHealth;
        this.health = maxHealth;
    }

    public void setHealth(float health){
        this.health = health;
    }

    public void damage(float damage){
        health -= damage;
    }

    public void heal(float heal){
        health += heal;
        if(health > maxHealth){
            health = maxHealth;
        }
    }
    public boolean isAlive(){
        return health > 0;
    }

    //Adders
    public void rotateToDirectionMoving(float offset){
        float rot = (float) Math.toDegrees(Math.atan2(-getSx(),getSy()));
        rotation = rot + offset;
    }

    public void addPosition(float deltax, float deltay){
        x += deltax;
        y += deltay;
    }
    public void addPosition(Vector2 delta){
        x += delta.x;
        y += delta.y;
    }

    public void addSpeed(Vector2 delta){
        addSpeed(delta.x,delta.y);
    }

    public void addSpeed(float dx, float dy){
        this.sx += dx;
        this.sy += dy;
    }

    public void setSpeed(Vector2 velocity){
        this.sx = velocity.x;
        this.sy = velocity.y;
    }

    public void rotate(float deltaRotation){
        rotation += deltaRotation;
    }

    //HitBox methods
    /**
     * This Method resolves a Collision so that two overlapping Objects do not
     * overlap anymore.
     * <p/>
     * This Method does not check for null anymore, that has to be done
     * beforehand.
     * <p/>
     * @param col The Collision that has occurred.
     */
    public static void resolveCollision(Collision col, boolean exertForce) {
        Entity obj1 = (Entity) col.getBox1().getParent();
        Entity obj2 = (Entity) col.getBox2().getParent();
        if (obj1.isSolid() && obj2.isSolid()) {
            Vector2 displacement;
            if (!obj1.fixated && !obj2.fixated) {
                float totalMass = obj1.getMass() + obj2.getMass();
                float factor1 = obj2.getMass() / totalMass;
                float factor2 = obj1.getMass() / totalMass;
                displacement = col.getDisplacement().cpy().scl(factor1,factor1);
                obj1.addPosition(displacement);
                if(exertForce){
                    obj1.addSpeed(displacement.scl(obj1.getBounciness()));
                }
                displacement = col.getDisplacement().cpy().scl(-factor2);
                obj2.addPosition(displacement);
                if(exertForce){
                    obj2.addSpeed(displacement.scl(obj2.getBounciness()));
                }
            } else if (!obj1.fixated) {
                displacement = col.getDisplacement();
                obj1.addPosition(displacement);
                if(exertForce){
                    obj1.addSpeed(displacement.scl(obj1.getBounciness()));
                }
            } else if (!obj2.fixated) {
                displacement = col.getDisplacement().scl(-1);
                obj2.addPosition(displacement);
                if(exertForce){
                    obj2.addSpeed(displacement.scl(obj2.getBounciness()));
                }
            } else {
                //This else will allow two static objekts to push each other.
                /*displacement = col.getDisplacement().div(2);
                obj1.addPosition(displacement);
                displacement = displacement.mul(-1);
                obj2.addPosition(displacement);*/
            }
        }
    }

    @Override
    public Entity getParent() {
        return this;
    }

    @Override
    public int getType() {
        return HitBox.AABB;
    }

    public Rectangle getBoundingBox() {
        if(hitBox != null){
            return new Rectangle(x+hitBox.x,y+hitBox.y,hitBox.getWidth(),hitBox.getHeight());
        }else{
            return new Rectangle(x,y,imgW,imgH);
        }
    }

    public float getBBWidth(){
        if(hitBox != null){
            return hitBox.width;
        }else{
            return imgW;
        }
    }

    public float getBBHeight(){
        if(hitBox != null){
            return hitBox.height;
        }else{
            return imgH;
        }
    }

    public float getBBBottom(){
        if(hitBox != null){
            return y+hitBox.y;
        }else{
            return y;
        }
    }

    public float getBBLeft(){
        if(hitBox != null){
            return x+hitBox.x;
        }else{
            return x;
        }
    }

    public Vector2 getCenter() {
        Rectangle rect = getBoundingBox();
        return new Vector2(rect.getX()+rect.getWidth()/2, rect.getY()+rect.getHeight()/2);
    }

    public Vector2 getVelocity(){
        return new Vector2(sx,sy);
    }

    public void setCenter(float x, float y) {
        this.x = x-(hitBox.x+hitBox.getWidth()/2);
        this.y = y-(hitBox.y+hitBox.getHeight()/2);
    }

    public void setCenter(Vector2 cent){
        setCenter(cent.x,cent.y);
    }

    @Override
    public float getRadius() {
        if(hitBox != null) {
            return hitBox.getWidth() / 2;
        }else{
            return imgW/2;
        }
    }

    private GrapplingHook hookedBy;

    public void hookThisEntity(GrapplingHook hook){
        this.hookedBy = hook;
    }

    public GrapplingHook getHookedBy() {
        return hookedBy;
    }

    //Getter & Setter
    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getSx() {
        return sx;
    }

    public void setSx(float sx) {
        this.sx = sx;
    }

    public float getSy() {
        return sy;
    }

    public void setSy(float sy) {
        this.sy = sy;
    }

    public float getGx() {
        return gx;
    }

    public void setGx(float gx) {
        this.gx = gx;
    }

    public float getGy() {
        return gy;
    }

    public void setGy(float gy) {
        this.gy = gy;
    }

    public float getFx() {
        return fx;
    }

    public void setFx(float fx) {
        this.fx = fx;
    }

    public float getFy() {
        return fy;
    }

    public void setFy(float fy) {
        this.fy = fy;
    }

    public float getRotation() {
        return rotation;
    }

    public void setRotation(float rotation) {
        this.rotation = rotation;
    }

    public boolean isSolid() {
        return solid;
    }

    public void setSolid(boolean solid) {
        this.solid = solid;
    }

    public boolean isFixated() {
        return fixated;
    }

    public void setFixated(boolean fixated) {
        this.fixated = fixated;
    }

    public float getMass() {
        return mass;
    }

    public void setMass(float mass) {
        this.mass = mass;
    }

    public float getBounciness() {
        return bounciness;
    }

    public void setBounciness(float bounciness) {
        this.bounciness = bounciness;
    }

    public boolean isFlipX() {
        return flipX;
    }

    public void setFlipX(boolean flipX) {
        this.flipX = flipX;
    }

    public boolean isFlipY() {
        return flipY;
    }

    public void setFlipY(boolean flipY) {
        this.flipY = flipY;
    }

    public void setDrawableSize(float width, float height, boolean updateHitbox){
        this.imgW = width;
        this.imgH = height;
        if(updateHitbox){
            this.hitBox = new Rectangle(0,0,imgW,imgH);
        }
    }

    public void rescale(float scaleX, float scaleY){
        Vector2 center = this.getCenter();
        this.imgW = imgW*scaleX;
        this.imgH = imgH*scaleY;
        this.setHitBox(new Rectangle(hitBox.x*scaleX,hitBox.y*scaleY,hitBox.width*scaleX,hitBox.height*scaleY));
        this.setCenter(center);
    }

    public float getMaxHealth() {
        return maxHealth;
    }

    public float getHealth() {
        return health;
    }

    public void setSpawnProtection(int spawnProtection) {
        this.spawnProtection = spawnProtection;
    }

    public void setHitBox(Rectangle hitBox) {
        this.hitBox = hitBox;
    }
    public boolean isForeground(){
        return false;
    }

    public boolean isHostile() {
        return hostile;
    }

    public void setHostile(boolean hostile) {
        this.hostile = hostile;
    }

    public Fanta getFanta() {
        return fanta;
    }

    public void setFanta(Fanta fanta, boolean updateDrawableSize, boolean updateHitBox) {
        this.fanta = fanta;
        if(updateDrawableSize && fanta != null){
            setDrawableSize(fanta.getCurrentFrame().getRegionWidth(), fanta.getCurrentFrame().getRegionHeight(), updateHitBox);
        }
    }

    public boolean isSpawnProtection() {
        return spawnProtection > 0;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public float getImgW() {
        return imgW;
    }

    public float getImgH() {
        return imgH;
    }

    public void placeOnPlanet(Entity planet){
        Vector2 delta = getCenter().sub(planet.getCenter());
        this.setRotation(delta.angle()-90);
        this.setCenter(planet.getCenter().add(delta.nor().scl(planet.getRadius() + getRadius())));
    }

    public abstract String getDataType();

    public boolean isDraggable() {
        return draggable;
    }

    public void setDraggable(boolean draggable) {
        this.draggable = draggable;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }
}
