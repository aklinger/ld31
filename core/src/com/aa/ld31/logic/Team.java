package com.aa.ld31.logic;

import com.badlogic.gdx.graphics.Color;

/**
 * Created by Toni on 07.12.2014.
 */
public enum Team {
    NONE("Streakers", 0, 0, Color.GRAY), ORANGE("Laser Kitties", 1, 1, Color.ORANGE), BLUE("Frosty Snowmen", 2, -1, new Color(0.2f, 0.7f, 0.9f, 1f));

    private String name;
    private int number;
    private int direction;
    private Color color;

    private Team(String name, int number, int direction, Color color) {
        this.name = name;
        this.number = number;
        this.direction = direction;
        this.color = color;
    }

    public int getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public int getDirection() {
        return direction;
    }

    public Color getColor() {
        return color;
    }
}
