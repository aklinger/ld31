package com.aa.ld31.logic.objects;

import com.aa.ld31.dal.audio.SoundFX;
import com.aa.ld31.dal.audio.SoundManager;
import com.aa.ld31.logic.EntityFactory;
import com.aa.ld31.logic.EntityManager;
import com.aa.ld31.logic.Fanta;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Toni on 06.12.2014.
 */
public class GrappleToken extends Token {

    public GrappleToken(Fanta fanta) {
        super(fanta);
        setMoveSpeed(1.1f);
        setKickSpeed(20);
        setSpecialCooldown(60 * 3);
    }

    @Override
    public void act(EntityManager man) {
        super.act(man);
    }

    @Override
    protected void useSpecial(EntityManager manager, Vector2 direction) {
        super.useSpecial(manager, direction);
        GrapplingHook hook = new GrapplingHook(this);
        if(direction == null || direction.isZero()) {
            Vector2 delta = manager.getBall().getCenter().sub(this.getCenter());
            hook.shootIntoDirection(delta);
        }else{
            hook.shootIntoDirection(direction);
        }
        manager.spawnEntity(hook);
        EntityFactory.createParticleBurst(this.getCenter(), 10, 8, 15, manager.getBall().getCenter().sub(this.getCenter()), 45, 5, 8, 0.9f, Color.GRAY, 8, manager);
        SoundManager.getInstance().play(SoundFX.HOOKSHOT, 0.9f);
    }
}
