package com.aa.ld31.logic.objects;

import com.aa.ld31.dal.TextureManager;
import com.aa.ld31.dal.audio.SoundFX;
import com.aa.ld31.dal.audio.SoundManager;
import com.aa.ld31.logic.*;
import com.aa.ld31.maths.Collision;
import com.aa.ld31.maths.HitBox;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Toni on 06.12.2014.
 */
public class Token extends BasicObject {

    private float moveSpeed = 1;
    private float kickSpeed = 15;
    private float ballKickReachDist = 30;

    private int specialCooldown = 60;
    private int specialTimer;

    private float ballAffectForce = 1.25f;
    private float ballAffectRadius = 26;
    private float ballKeepDistance = 20;

    private int soundFXbugfix = 0;

    private Team team = Team.NONE;

    public Token(Fanta fanta) {
        super("Token", fanta);
        setFx(0.8f);
        setFy(0.8f);
        setMass(8f);
    }

    @Override
    public void act(EntityManager man) {
        super.act(man);
        if(specialTimer>0) {
            specialTimer--;
        }
        soundFXbugfix--;
    }

    public boolean isSpecialReady(){
        return specialTimer<=0;
    }

    protected void useSpecial(EntityManager manager, Vector2 direction){
        specialTimer = specialCooldown;
    }

    public void tryToUseSpecial(EntityManager manager, Vector2 direction){
        if(isSpecialReady()){
            useSpecial(manager, direction);
        }
    }

    public void moveInDirection(EntityManager man, Vector2 direction){
        if(!direction.isZero()){
            this.addSpeed(direction.limit(1).scl(moveSpeed));
        }
    }

    protected void kickBall(EntityManager man){
        Entity ball = man.getBall();
        Vector2 deltaVector = ball.getCenter().sub(this.getCenter());
        float reachPlusRadius = getImgW()/2+ ballKickReachDist;
        if(deltaVector.len2()<reachPlusRadius*reachPlusRadius){
            ball.setSpeed(deltaVector.nor().scl(kickSpeed));
            if(soundFXbugfix <= 0) {
                SoundManager.getInstance().playRandom(0.3f, SoundFX.KICKS);
                soundFXbugfix = 5;
            }
        }
    }

    @Override
    public void render(SpriteBatch batch) {
        super.render(batch);
        TextureRegion block = TextureManager.getInstance().getTexture("block");
        batch.draw(block, getX(), getY(), 64, 5);
        TextureRegion floor = TextureManager.getInstance().getTexture("floor");
        batch.draw(floor,getX(),getY(), 64*getCooldownPercent(), 5);
    }


    @Override
    public void interactWith(Entity ent2, Collision col, EntityManager man) {
        super.interactWith(ent2, col, man);
        if(ent2 instanceof Ball){
            float affectPlusRadius = getImgW()/2+ballAffectRadius;
            if(ent2.getCenter().sub(this.getCenter()).len2() < affectPlusRadius * affectPlusRadius) {
                Vector2 velocity = getVelocity();
                Vector2 frontside = getCenter().add(velocity.nor().scl(getImgW()/2+ballKeepDistance));
                frontside.sub(ent2.getCenter()).nor().scl(ballAffectForce);
                ent2.addSpeed(frontside);
            }
        }
    }

    private float getCooldownPercent(){
        return (specialTimer/(float)specialCooldown);
    }

    public int getPlayerIndex(){
        return getPlayerFromTeam(team);
    }

    public int getPlayerFromTeam(Team team){
        if(team == Team.ORANGE){
            return 0;
        }else if(team == Team.BLUE){
            return 1;
        }
        return 0;
    }

    @Override
    public int getType() {
        return HitBox.CIRCLE;
    }

    public void setMoveSpeed(float moveSpeed) {
        this.moveSpeed = moveSpeed;
    }

    public void setKickSpeed(float kickSpeed) {
        this.kickSpeed = kickSpeed;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public void setBallKickReachDist(float ballKickReachDist) {
        this.ballKickReachDist = ballKickReachDist;
    }

    public void setSpecialCooldown(int specialCooldown) {
        this.specialCooldown = specialCooldown;
    }

    public void setBallAffectForce(float ballAffectForce) {
        this.ballAffectForce = ballAffectForce;
    }

    public void setBallAffectRadius(float ballAffectRadius) {
        this.ballAffectRadius = ballAffectRadius;
    }

    public void setBallKeepDistance(float ballKeepDistance) {
        this.ballKeepDistance = ballKeepDistance;
    }

    public float getBallAffectRadius() {
        return ballAffectRadius;
    }
}
