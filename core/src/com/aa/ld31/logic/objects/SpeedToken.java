package com.aa.ld31.logic.objects;

import com.aa.ld31.dal.audio.SoundFX;
import com.aa.ld31.dal.audio.SoundManager;
import com.aa.ld31.logic.EntityManager;
import com.aa.ld31.logic.Fanta;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Toni on 06.12.2014.
 */
public class SpeedToken extends Token {

    private float dashDistance = 110;

    public SpeedToken(Fanta fanta) {
        super(fanta);
        setFx(0.75f);
        setFy(0.75f);
        setMoveSpeed(1.8f);
        setKickSpeed(15);
        setMass(3f);
        setSpecialCooldown(60 * 6);
    }

    @Override
    public void act(EntityManager man) {
        super.act(man);
    }

    @Override
    protected void useSpecial(EntityManager man, Vector2 direction) {
        super.useSpecial(man, direction);
        hookThisEntity(null);
        Vector2 delta;
        if(direction == null || direction.isZero()){
            delta = man.getBall().getCenter().sub(this.getCenter());
        }else{
            delta = direction;
        }
        man.dash(this, delta, dashDistance, getImgW() / 2 + getBallAffectRadius());
        SoundManager.getInstance().play(SoundFX.DASH, 0.9f);
    }
}
