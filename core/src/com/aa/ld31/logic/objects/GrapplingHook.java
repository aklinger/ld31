package com.aa.ld31.logic.objects;

import com.aa.ld31.dal.TextureManager;
import com.aa.ld31.dal.audio.SoundFX;
import com.aa.ld31.dal.audio.SoundManager;
import com.aa.ld31.logic.*;
import com.aa.ld31.maths.Collision;
import com.aa.ld31.maths.HitBox;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.utils.TiledDrawable;

/**
 * Created by Toni on 06.12.2014.
 */
public class GrapplingHook extends BasicObject{

    private Entity hooked;
    private Entity source;

    private float chainMaxLength = 550f;
    private float chainSpeed = 20f;
    private float reelSpeed = 4f;
    private float chainCurrentLength;
    private float maxFlyDuration = 60;
    private float flyTime = 0;
    private float hookTime = 0;
    private float minStunDuration = 60;

    public GrapplingHook(Entity source) {
        super("GrapplingHook", createFanta());
        setDrawableSize(30,30,true);
        setSolid(false);
        this.source = source;
    }

    @Override
    public void act(EntityManager man) {
        super.act(man);
        this.rotateToDirectionMoving(0);
        if(hooked != null){
            if(hooked.getHookedBy() != this){
                man.killEntity(this);
            }else {
                hookTime++;
                chainCurrentLength -= reelSpeed;
                if (chainCurrentLength < source.getImgW()/2+hooked.getImgW()/2 + 5) {
                    chainCurrentLength = source.getImgW()/2+hooked.getImgW()/2 + 5;
                }
                Vector2 diff = hooked.getCenter().sub(source.getCenter()).nor().scl(chainCurrentLength);
                Vector2 hookPosition = source.getCenter().add(diff);
                hooked.setCenter(hookPosition);
                this.setRotation(diff.angle() - 90);
                this.setCenter(hookPosition.add(diff.nor().scl(-hooked.getImgW() / 2)));
                if (hookTime >= minStunDuration && chainCurrentLength <= source.getImgW()/2+hooked.getImgW()/2 + 5) {
                    man.killEntity(this);
                }
            }
        }else{
            flyTime++;
            if(flyTime > maxFlyDuration || this.getCenter().sub(source.getCenter()).len() > chainMaxLength){
                man.killEntity(this);
            }
        }
    }

    public void shootIntoDirection(Vector2 direction){
        hooked = null;
        flyTime = 0;
        this.setCenter(source.getCenter());
        this.setSpeed(direction.nor().scl(chainSpeed));
        this.getFanta().setCurrentAnimation("default");
        this.rotateToDirectionMoving(0);
    }

    @Override
    public void interactWith(Entity ent2, Collision col, EntityManager man) {
        super.interactWith(ent2, col, man);
        if(col != null && hooked == null){
            if(ent2 != source && !(ent2 instanceof GrapplingHook)){
                if(!(ent2 instanceof Shield) && !((ent2 instanceof ShieldToken) && ((ShieldToken) ent2).isLarge())) {
                    hooked = ent2;
                    ent2.hookThisEntity(this);
                    this.getFanta().setCurrentAnimation("hooked");
                    chainCurrentLength = ent2.getCenter().sub(source.getCenter()).len();
                    this.setSpeed(Vector2.Zero);
                    EntityFactory.createParticleBurst(this.getCenter(), 4, 5, 8, Vector2.Zero, 360, 1, 3, 0.88f, Color.GRAY, 8, man);
                    SoundManager.getInstance().play(SoundFX.HOOKSHOT_HIT, 0.9f);
                    hookTime = 0;
                }else{
                    man.killEntity(this);
                }
            }
        }
    }

    @Override
    public int getType() {
        return HitBox.CIRCLE;
    }

    @Override
    public void render(SpriteBatch batch) {
        super.render(batch);
        if(source != null){
            float distance = source.getCenter().dst(getCenter())-14;
            float angle = getCenter().sub(source.getCenter()).angle()-90;
            TextureRegion hookChain = TextureManager.getInstance().getTexture("hook_chain");
            TiledDrawable tiledDrawable = new TiledDrawable(hookChain);
            int size = 16;
            batch.setColor(Color.WHITE);
            tiledDrawable.draw(batch, source.getCenter().x-size/2,source.getCenter().y,size/2,0,size,size,1,(distance/size),angle);
        }
    }

    private static Fanta createFanta(){
        Fanta fanta = new Fanta("default");

        Animation anim = new Animation(50, EntityFactory.loadSingleFrame("grapplingHook1"));
        anim.setPlayMode(Animation.PlayMode.LOOP);
        fanta.putAnimation("default", anim);

        anim = new Animation(50, EntityFactory.loadSingleFrame("grapplingHook2"));
        anim.setPlayMode(Animation.PlayMode.LOOP);
        fanta.putAnimation("hooked", anim);
        return fanta;
    }

    public Entity getSource() {
        return source;
    }
}
