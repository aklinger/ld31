package com.aa.ld31.logic.objects;

import com.aa.ld31.dal.audio.SoundFX;
import com.aa.ld31.dal.audio.SoundManager;
import com.aa.ld31.logic.EntityFactory;
import com.aa.ld31.logic.EntityManager;
import com.aa.ld31.logic.Fanta;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Toni on 06.12.2014.
 */
public class ShieldToken extends Token {

    private int shieldTime = 60*5;
    private int shieldTimer;

    private float normalMoveSpeed = 1.45f;
    private float largeMoveSpeed = 0.666f;
    private int normalSize = 64;
    private int shieldSize = 128;
    private float normalShootSpeed = 25;
    private float largeShootSpeed = 50;
    private float normalMass = 15;
    private float largeMass = 100;

    public ShieldToken(Fanta fanta) {
        super(fanta);
        setMoveSpeed(normalMoveSpeed);
        setKickSpeed(normalShootSpeed);
        setMass(normalMass);
        setSpecialCooldown(60 * 10);
    }

    @Override
    public void act(EntityManager man) {
        super.act(man);
        checkIfSpecialIsOver(man);
        if(isLarge()){
            kickBall(man);
        }
    }
    private void checkIfSpecialIsOver(EntityManager manager) {
        shieldTimer--;
        if(shieldTimer==0){
            Vector2 center = getCenter();
            this.setDrawableSize(normalSize, normalSize, true);
            this.setCenter(center);
            this.setMoveSpeed(normalMoveSpeed);
            this.setKickSpeed(normalShootSpeed);
            this.setMass(normalMass);
        }
    }

    @Override
    protected void useSpecial(EntityManager manager, Vector2 direction) {
        super.useSpecial(manager, direction);
        shieldTimer = shieldTime;
        Vector2 center = getCenter();
        this.setDrawableSize(shieldSize, shieldSize, true);
        this.setCenter(center);
        this.setMoveSpeed(largeMoveSpeed);
        this.setKickSpeed(largeShootSpeed);
        this.setMass(largeMass);
        this.hookThisEntity(null);
        EntityFactory.createParticleBurst(this.getCenter(), 4, 10, 20, Vector2.Zero, 360, 12, 12, 0.85f, Color.GRAY, 8, manager);
        manager.shakeScreen(5);
        SoundManager.getInstance().play(SoundFX.GROW, 0.9f);
    }

    public boolean isLarge(){
        return shieldTimer > 0;
    }
}
