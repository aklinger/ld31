package com.aa.ld31.logic.objects;

import com.aa.ld31.logic.BasicObject;
import com.aa.ld31.logic.EntityManager;
import com.aa.ld31.logic.Fanta;
import com.aa.ld31.maths.HitBox;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Toni on 06.12.2014.
 */
public class Ball extends BasicObject {

    private Vector2 anchored;

    public Ball(Fanta fanta) {
        super("Ball", fanta);
        setFy(0.9f);
        setFx(0.9f);
        setMass(1f);
        sideWayBorders = false;
    }

    @Override
    public void act(EntityManager man) {
        super.act(man);
        keepBallFromBorders(man);
        if(anchored != null){
            this.setCenter(anchored);
        }
        rotateToDirectionMoving(90);
        float velocity = getVelocity().len();
        if(velocity > 8){
            getFanta().setCurrentAnimation("fast");
        }else if(velocity > 1){
            getFanta().setCurrentAnimation("rolling");
        }else{
            getFanta().setCurrentAnimation("still");
        }
    }

    private float border = 64;
    private float borderPushBack = 1f;
    private void keepBallFromBorders(EntityManager manager){
        if(sideWayBorders) {
            if (getBBLeft() < border) {
                addSpeed(borderPushBack, 0);
            } else if (getBBLeft() + getBBWidth() > manager.getGameAreaWidth() - border) {
                addSpeed(-borderPushBack, 0);
            }
        }
        if(getBBBottom()< border){
            addSpeed(0,borderPushBack);
        }else if(getBBBottom()+getBBHeight() > manager.getGameAreaHeight()-border){
            addSpeed(0,-borderPushBack);
        }
    }

    @Override
    public int getType() {
        return HitBox.CIRCLE;
    }

    public void anchorInPlace(){
        anchored = this.getCenter();
    }

    public void releaseAnchor(){
        anchored = null;
        hookThisEntity(null);
    }

    public boolean isAnchored(){
        return anchored != null;
    }
}
