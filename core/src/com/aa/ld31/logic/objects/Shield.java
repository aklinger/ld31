package com.aa.ld31.logic.objects;

import com.aa.ld31.logic.*;
import com.aa.ld31.maths.HitBox;
import com.badlogic.gdx.graphics.g2d.Animation;

/**
 * Created by Toni on 06.12.2014.
 */
public class Shield extends BasicObject{

    private int lifeTime;
    private int timer;

    private Entity source;

    public Shield(Entity source, float size, int lifeTime) {
        super("Shield", createFanta());
        setDrawableSize(size, size, true);
        setFixated(true);
        this.lifeTime = lifeTime;
        this.source = source;
    }

    @Override
    public void act(EntityManager man) {
        super.act(man);
        timer++;
        if(timer > lifeTime){
            man.killEntity(this);
        }
    }

    @Override
    public boolean collidesWith(Entity other) {
        return other != source && super.collidesWith(other);
    }

    @Override
    public int getType() {
        return HitBox.CIRCLE;
    }

    private static Fanta createFanta(){
        Fanta fanta = new Fanta("default");

        Animation anim = new Animation(50, EntityFactory.loadSingleFrame("shield"));
        anim.setPlayMode(Animation.PlayMode.LOOP);
        fanta.putAnimation("default", anim);

        return fanta;
    }
}
