package com.aa.ld31.logic.objects;

import com.aa.ld31.dal.audio.SoundFX;
import com.aa.ld31.dal.audio.SoundManager;
import com.aa.ld31.logic.Entity;
import com.aa.ld31.logic.EntityFactory;
import com.aa.ld31.logic.EntityManager;
import com.aa.ld31.logic.Fanta;
import com.aa.ld31.maths.Collision;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Toni on 06.12.2014.
 */
public class MagnetToken extends Token{

    private float attractionForce = 0f;
    private float attractionRadius = 50f;

    private float pushBackForce = 35f;
    private float pushBackRadius = 350f;

    public MagnetToken(Fanta fanta) {
        super(fanta);
        setMoveSpeed(1.2f);
        setKickSpeed(17);
        setSpecialCooldown(60*5);
        setBallAffectForce(1.61f);
        setBallAffectRadius(28);
    }

    @Override
    public void act(EntityManager man) {
        super.act(man);
    }

    @Override
    protected void useSpecial(EntityManager manager, Vector2 direction) {
        super.useSpecial(manager, direction);
        hookThisEntity(null);
        manager.pushBack(this.getCenter(), pushBackForce, pushBackRadius);
        EntityFactory.createParticleBurst(this.getCenter(), 4, 10, 18, Vector2.Zero, 360, 9, 9, 0.9f, Color.GRAY, 8, manager);
        manager.shakeScreen(3);
        EntityFactory.createShockWave(this.getCenter(), manager);
        SoundManager.getInstance().play(SoundFX.SHOCKWAVE, 0.9f);
    }

    @Override
    public void interactWith(Entity ent2, Collision col, EntityManager man) {
        super.interactWith(ent2, col, man);
        if(ent2 instanceof Ball){
            Vector2 deltaVector = this.getCenter().sub(ent2.getCenter());
            if(deltaVector.len2() < attractionRadius*attractionRadius){
                float forcePercent = (1-deltaVector.len2()/(attractionRadius*attractionRadius)); //Quadratic decay
                ent2.addSpeed(deltaVector.nor().scl(forcePercent*attractionForce));
            }
        }
    }
}
