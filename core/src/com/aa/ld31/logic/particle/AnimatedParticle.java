package com.aa.ld31.logic.particle;

import com.aa.ld31.gl.SmoothFont;
import com.aa.ld31.logic.Entity;
import com.aa.ld31.logic.EntityManager;
import com.aa.ld31.logic.Fanta;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Toni on 27.04.2014.
 */
public class AnimatedParticle extends Entity implements Particle{

    private float duration = -1;
    private float time = 0;

    private boolean lazor;
    private boolean fadeOut;

    private float growMaxSize;

    private String text;
    private BitmapFont font;

    public AnimatedParticle (Fanta f){
        setFanta(f, true, true);
    }

    public AnimatedParticle (String text, BitmapFont font){
        this.text = text;
        this.font = font;
    }

    @Override
    public void act() {
        if(getFanta() != null) {
            getFanta().act(1);
        }
        computeMovement();
        time++;

        if(fadeOut){
            this.getColor().a = 1-time/duration;
        }
        if(growMaxSize > 0){
            float growDelta = (growMaxSize-getImgW())/2;
            Vector2 center = this.getCenter();
            this.setDrawableSize(getImgW()+growDelta, getImgH()+growDelta, true);
            this.setCenter(center);
        }
    }

    @Override
    public boolean isActive() {
        if(duration < 0 || time>duration) {
            return getFanta() != null && !getFanta().isAnimationFinished();
        }
        return true;
    }

    @Override
    public void render(SpriteBatch batch) {
        if(text != null){
            batch.setColor(getColor());
            SmoothFont.renderSmoothCentered(font, batch, getX(), getY(), text);
            batch.setColor(Color.WHITE);
        }else {
            super.render(batch);
        }
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }

    @Override
    public void act(EntityManager man) {
        act();
    }

    @Override
    public String getDataType() {
        return null;
    }

    public void setLazor(boolean lazor) {
        this.lazor = lazor;
    }

    @Override
    public boolean isLazor() {
        return lazor;
    }

    public void setFadeOut(boolean fadeOut) {
        this.fadeOut = fadeOut;
    }

    public void setGrowMaxSize(float growMaxSize) {
        this.growMaxSize = growMaxSize;
    }
}
