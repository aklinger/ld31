package com.aa.ld31.logic.particle;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * A Particle. (Something that looks pretty)
 * <p/>
 * Created by Toni on 12.07.2014.
 */
public interface Particle {
    //Methoden

    /**
     * That what should happen every frame.
     */
    public void act();

    /**
     * Renders the Particle.
     *
     * @param batch The SpriteBatch to use for rendering.
     */
    public void render(SpriteBatch batch);

    /**
     * If the Particle is still active.
     *
     * @return False if the Particle can be removed.
     */
    public boolean isActive();

    /**
     * If additive blending should be used.
     * @return True if additive blending should be used, false otherwise.
     */
    public boolean isLazor();
}