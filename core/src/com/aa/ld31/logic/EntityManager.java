package com.aa.ld31.logic;

/**
 * Created by Toni on 16.04.2014.
 */

import aurelienribon.tweenengine.*;
import com.aa.ld31.LD31Game;
import com.aa.ld31.ai.AI;
import com.aa.ld31.dal.TextureManager;
import com.aa.ld31.dal.audio.SoundFX;
import com.aa.ld31.dal.audio.SoundManager;
import com.aa.ld31.dal.input.InputAction;
import com.aa.ld31.dal.input.PlayerInputController;
import com.aa.ld31.gl.SmoothFont;
import com.aa.ld31.logic.objects.Ball;
import com.aa.ld31.logic.objects.Shield;
import com.aa.ld31.logic.objects.Token;
import com.aa.ld31.logic.particle.AnimatedParticle;
import com.aa.ld31.logic.particle.ParticleList;
import com.aa.ld31.logic.particle.ParticleManager;
import com.aa.ld31.maths.Collision;
import com.aa.ld31.maths.CollisionUtil;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Manages all entities that are in the game at any given moment.
 *
 * @author Toni
 */
public class EntityManager {
    //Attribute
    private List<Entity> entities;
    private List<Entity> inbox;
    private List<Entity> trash;
    private ParticleManager particles;

    private OrthographicCamera camera;

    private int gameAreaWidth;
    private int gameAreaHeight;

    private ScreenShake screenShake;
    private Entity cameraFocus;

    private Ball ball;

    private static final int GOAL_SIZE = 400;
    private Sprite leftSide;
    private Sprite rightSide;
    private Sprite leftSide2;
    private Sprite rightSide2;

    private List<TeamInfo> teams;

    private List<AI> ais;

    private String message;
    private int scoreLimit = 100;
    private Team winningTeam = null;

    private boolean tutorial = false;
    private int tutorialIndex = 0;

    private TweenManager tweenManager;

    //Constants
    public static final int DEFAULT_VIEWPORT_WIDTH = 1280;
    public static final int DEFAULT_VIEWPORT_HEIGHT = 720;

    //Constructor
    public EntityManager() {
        entities = new ArrayList<>();
        inbox = new ArrayList<>();
        trash = new ArrayList<>();
        particles = new ParticleList();

        this.gameAreaWidth = DEFAULT_VIEWPORT_WIDTH;
        this.gameAreaHeight = DEFAULT_VIEWPORT_HEIGHT;

        camera = new OrthographicCamera(DEFAULT_VIEWPORT_WIDTH, DEFAULT_VIEWPORT_HEIGHT);
        camera.translate(camera.viewportWidth / 2, camera.viewportHeight / 2);
        camera.update();

        screenShake = new ScreenShake(1, 0.99f);

        initGoals();

        teams = new ArrayList<>(2);
        teams.add(new TeamInfo(Team.ORANGE));
        teams.add(new TeamInfo(Team.BLUE));

        ais = new ArrayList<>(2);

        tweenManager = new TweenManager();
    }

    private static final Color BLUE_TEAM_GOAL_COLOR = new Color(0.5f, 0.7f, 0.9f, 1);
    private static final Color ORANGE_TEAM_GOAL_COLOR = new Color(0.9f,0.7f,0.5f,1);
    private static final Color BLUE_TEAM_GOAL_COLOR_2 = new Color(0.5f*0.75f, 0.7f*0.75f, 0.9f*0.75f, 1);
    private static final Color ORANGE_TEAM_GOAL_COLOR_2 = new Color(0.9f*0.75f,0.7f*0.75f,0.5f*0.75f, 1);

    private void initGoals(){
        leftSide = new Sprite(TextureManager.getInstance().getTexture("block"));
        leftSide.setColor(ORANGE_TEAM_GOAL_COLOR);
        leftSide.setSize(GOAL_SIZE, gameAreaHeight);

        rightSide = new Sprite(TextureManager.getInstance().getTexture("block"));
        rightSide.setColor(BLUE_TEAM_GOAL_COLOR);
        rightSide.setSize(GOAL_SIZE, gameAreaHeight);
        rightSide.setPosition(gameAreaWidth - GOAL_SIZE, 0);

        leftSide2 = new Sprite(TextureManager.getInstance().getTexture("block"));
        leftSide2.setColor(ORANGE_TEAM_GOAL_COLOR_2);
        leftSide2.setSize(14, gameAreaHeight);

        rightSide2 = new Sprite(TextureManager.getInstance().getTexture("block"));
        rightSide2.setColor(BLUE_TEAM_GOAL_COLOR_2);
        rightSide2.setSize(14, gameAreaHeight);
        rightSide2.setPosition(gameAreaWidth - 14, 0);
    }

    public void initSomeStuff(){
        updateCamera();
        addInbox();
        if(tutorial) {
            despawnBall(60 * 4, true, 60*4);
        }else{
            despawnBall(60 * 3, true, 60);
        }
    }

    //Methoden
    public void act() {
        playerProcessInput();
        aisThink();

        addInbox();
        actEntities();
        removeTrash();

        actParticles();

        screenShake.update();
        updateCamera();
        if(winningTeam == null) {
            checkGoalCounter();
        }
        tweenManager.update(1);
    }

    private void playerProcessInput(){
        for(TeamInfo teamInfo : teams){
            teamInfo.handleInput(this);
        }
    }

    private void aisThink(){
        for(AI ai : ais){
            ai.think(this);
        }
    }

    private void checkGoalCounter(){
        if(ball.getX()<GOAL_SIZE){
            addScore(1, Team.BLUE);
            if(ball.getCenter().x < 0){
                addScore(60*10, Team.BLUE);
                EntityFactory.createTextParticle(ball.getCenter().add(40, 0), Team.BLUE.getColor(), "+10", this);
                if(winningTeam == null) {
                    despawnBall(30 * 3, false, 0);
                }
            }
            EntityFactory.createParticleBurst(ball.getCenter(), 8, 1, 5, Vector2.Zero, 360, 1, 3, 0.95f, Team.BLUE.getColor(), 8, this);
        }else if(ball.getX()>getGameAreaWidth()-GOAL_SIZE){
            addScore(1, Team.ORANGE);
            if(ball.getCenter().x > getGameAreaWidth()){
                addScore(60*10, Team.ORANGE);
                EntityFactory.createTextParticle(ball.getCenter().add(-40, 0), Team.ORANGE.getColor(), "+10", this);
                if(winningTeam == null) {
                    despawnBall(30 * 3, false, 0);
                }
            }
            EntityFactory.createParticleBurst(ball.getCenter(), 8, 1, 5, Vector2.Zero, 360, 1, 3, 0.95f, Team.ORANGE.getColor(), 8, this);
        }
    }

    private void addScore(int score, Team team){
        int reasonableScore = getReasonableScore(team);
        for(TeamInfo teamInfo : teams) {
            if (teamInfo.getTeam() == team) {
                teamInfo.addScore(score);
            }
        }
        if(score == 1 && getReasonableScore(team) > reasonableScore){
            EntityFactory.createTextParticle(new Vector2(gameAreaWidth/2-(gameAreaWidth/2-50)*team.getDirection(), 120), team.getColor(), "+1", this);
        }
        if(getReasonableScore(team) >= scoreLimit && scoreLimit > 0){
            winningTeam = team;
            TweenCallback fireworks = new TweenCallback() {
                @Override
                public void onEvent(int type, BaseTween<?> source) {
                    SoundManager.getInstance().playRandom(0.3f, SoundFX.FIREWORKS);
                    EntityFactory.createFireworks((float)(Math.random() * gameAreaWidth), (float)(Math.random() * gameAreaHeight/4)+gameAreaHeight*(0.75f), winningTeam.getColor(), EntityManager.this);
                }
            };
            Timeline.createSequence()
                    .push(Tween.call(fireworks))
                    .pushPause(5)
                    .push(Tween.call(fireworks))
                    .pushPause(10)
                    .push(Tween.call(fireworks))
                    .pushPause(10)
                    .push(Tween.call(fireworks))
                    .pushPause(5)
                    .push(Tween.call(fireworks))
                    .pushPause(5)
                    .push(Tween.call(fireworks))
                    .pushPause(30)
                    .push(Tween.call(fireworks))
                    .pushPause(5)
                    .push(Tween.call(fireworks))
                    .pushPause(40)
                    .push(Tween.call(fireworks))
                    .pushPause(5)
                    .push(Tween.call(fireworks))
                    .pushPause(20)
                    .push(Tween.call(fireworks))
                    .pushPause(5)
                    .push(Tween.call(fireworks))
                    .pushPause(5)
                    .push(Tween.call(fireworks))
                    .pushPause(20)
                    .push(Tween.call(fireworks))
                    .pushPause(5)
                    .push(Tween.call(fireworks))
                    .pushPause(20)
                    .push(Tween.call(fireworks))
                    .pushPause(5)
                    .push(Tween.call(fireworks))
                    .pushPause(20)
                    .push(Tween.call(fireworks))
                    .pushPause(5)
                    .push(Tween.call(fireworks))
                    .repeat(20, 0)
                    .start(tweenManager);
        }
    }

    private void respawnBall(){
        ball.releaseAnchor();
    }

    private void despawnBall(int time, boolean beginning, int delay){
        EntityFactory.createParticleBurst(ball.getCenter(), 10, 12, 8, Vector2.Zero, 360, 2, 4, 0.88f, Color.GRAY, 8, this);

        ball.setCenter(gameAreaWidth / 2, gameAreaHeight / 2);
        ball.setSpeed(Vector2.Zero);
        ball.anchorInPlace();

        Shield shield = new Shield(ball, 128, time+delay);
        shield.setCenter(ball.getCenter());
        spawnEntity(shield);

        TweenCallback respawnBallCallback = new TweenCallback() {
            @Override
            public void onEvent(int type, BaseTween<?> source) {
                respawnBall();
            }
        };
        String text = "Next round in ";
        if(beginning){
            text = "Match starts in ";
        }
        Timeline.createSequence()
                .pushPause(delay)
                .push(Tween.call(createMessageEvent(text + "3")))
                .pushPause(time/3)
                .push(Tween.call(createMessageEvent(text+"2")))
                .pushPause(time/3)
                .push(Tween.call(createMessageEvent(text+"1")))
                .pushPause(time/3)
                .push(Tween.call(createMessageEvent("GO!!!")))
                .push(Tween.call(respawnBallCallback))
                .pushPause(time/5)
                .push(Tween.call(createMessageEvent("")))
                .start(tweenManager);
    }

    private TweenCallback createMessageEvent(final String text){
        return new TweenCallback() {
            @Override
            public void onEvent(int type, BaseTween<?> source) {
                message = text;
            }
        };
    }

    public void addPlayerInputControllerToTeam(PlayerInputController playerInputController, Team team) {
        for(TeamInfo teamInfo : teams) {
            if (teamInfo.getTeam() == team) {
                teamInfo.addPlayerInputController(playerInputController);
            }
        }
    }

    public int getReasonableScore(Team team){
        for(TeamInfo teamInfo : teams) {
            if (teamInfo.getTeam() == team) {
                return teamInfo.getScore()/60;
            }
        }
        return 0;
    }

    private void actParticles() {
        particles.act();
    }

    private void actEntities() {
        for (int i = 0; i < entities.size(); i++) {
            Entity ent1 = entities.get(i);
            for (int j = i + 1; j < entities.size(); j++) {
                Entity ent2 = entities.get(j);
                Collision col = CollisionUtil.calcCollisionHitBoxes(ent1, ent2, true, false);
                if (ent1.collidesWith(ent2) && ent2.collidesWith(ent1) && col != null) {
                    Entity.resolveCollision(col, !(ent1.isSpawnProtection() || ent2.isSpawnProtection()));
                }
                ent1.interactWith(ent2, col, this);
                ent2.interactWith(ent1, col, this);
            }
            ent1.act(this);
            if (!ent1.isAlive()) {
                killEntity(ent1);
            }
        }
        for (Entity ent1 : entities) {
            ent1.afterAct(this);
        }
    }

    private void addInbox() {
        if (!inbox.isEmpty()) {
            addEntities(inbox);
            inbox.clear();
        }
    }

    private void removeTrash() {
        if (!trash.isEmpty()) {
            removeEntities(trash);
            trash.clear();
        }
    }

    public void prevTutorial(){
        tutorialIndex--;
        if(tutorialIndex < 0){
            tutorialIndex = 0;
        }
    }

    public void nextTutorial(){
        tutorialIndex++;
        if(tutorialIndex >= tutorialText.length){
            tutorialIndex = tutorialText.length - 1;
        }
    }

    public boolean isOnFirstPage(){
        return tutorialIndex == 0;
    }

    public boolean isOnLastPage(){
        return tutorialIndex == tutorialText.length - 1;
    }

    private String[][] tutorialText = {{"Get the ball into the opposing team's goal to score 10 points.", "You also gain 1 point for every second the ball is on their side of the field.", "First team with a score of "+scoreLimit+" wins!"},
            {"You automatically try to pick up the ball when moving near it.", "The bar below your unit shows if your special is ready.", "Don't forget you can also switch your unit at any time."},
            {"Runner - Can instantly dash a short distance", "Magnet - Can create a shockwave", "Grappler - Can pull stuff to him with a hookshot", "Shield - Can grow and get strong but slow for a short period of time"},
            {"Press P or ESC to pause", "Use the button below to exit to Main Menu.\n        Good luck and have fun!"}};

    private StringBuffer tutText = new StringBuffer();
    private String getTutorialMessage(){
        tutText.delete(0,tutText.length());
        for(int i = 0; i < tutorialText[tutorialIndex].length; i++){
            tutText.append(tutorialText[tutorialIndex][i]);
            tutText.append("\n\n");
        }
        return tutText.toString();
    }

    public void render(SpriteBatch batch) {
        leftSide.draw(batch);
        rightSide.draw(batch);
        leftSide2.draw(batch);
        rightSide2.draw(batch);

        for(TeamInfo teamInfo : teams) {
            Map<PlayerInputController, Token> selections = teamInfo.getSelections();
            for(PlayerInputController player : selections.keySet()) {
                Token selectedToken = selections.get(player);
                if (selectedToken != null) {
                    TeamInfo.TempPlayerData tempPlayerData = teamInfo.getTempPlayerData(player);
                    TextureRegion selection = TextureManager.getInstance().getTexture("selection");
                    TextureRegion selection_2 = TextureManager.getInstance().getTexture("selection2");
                    float selectionSize = selectedToken.getImgW()+32;
                    float miniSize = 4;
                    float xInput = (player.getSliderValue(InputAction.ACTION_MOVE_RIGHT) - player.getSliderValue(InputAction.ACTION_MOVE_LEFT));
                    float yInput = (player.getSliderValue(InputAction.ACTION_MOVE_UP) - player.getSliderValue(InputAction.ACTION_MOVE_DOWN));
                    batch.setColor(player.getColor());
                    batch.draw(selection, tempPlayerData.getSelectionPos().x - selectionSize / 2, tempPlayerData.getSelectionPos().y - selectionSize / 2, selectionSize, selectionSize);
                    batch.setColor(Color.GRAY);
                    batch.draw(selection, selectedToken.getCenter().x - miniSize/2 + xInput * selectionSize/2, selectedToken.getCenter().y - miniSize / 2 + yInput * selectionSize / 2, miniSize, miniSize);

                    Token closestToken = teamInfo.getClosestTokenInDirection(player, new Vector2(xInput,yInput));
                    if(closestToken != null){
                        selectionSize = closestToken.getImgW()+32;
                        batch.setColor(player.getBrighterColor());
                        batch.draw(selection_2, closestToken.getCenter().x - selectionSize / 2, closestToken.getCenter().y - selectionSize / 2, selectionSize/2, selectionSize/2, selectionSize, selectionSize, 1, 1, tempPlayerData.getSelectionRot());
                    }
                }
            }
        }

        for (Entity ent : entities) {
            if (!ent.isForeground()) {
                ent.render(batch);
            }
        }
        particles.render(batch);
        for (Entity ent : entities) {
            if (ent.isForeground()) {
                ent.render(batch);
            }
        }

        for(TeamInfo teamInfo : teams) {
            Map<PlayerInputController, Token> selections = teamInfo.getSelections();
            for(PlayerInputController player : selections.keySet()) {
                Token selectedToken = selections.get(player);
                if (selectedToken != null) {
                    SmoothFont.renderSmooth(SmoothFont.getSmoothFont_small_black(), batch, selectedToken.getCenter().x - 15, selectedToken.getCenter().y + selectedToken.getImgH() / 2 + 15, player.getName());
                }
            }
        }

        if(tutorial){
            SmoothFont.renderSmooth(SmoothFont.getSmoothFont_small_black(), batch, gameAreaWidth /2 - 375, gameAreaHeight/2 - 10, getTutorialMessage());
        }

        if(message != null && !message.isEmpty()){
            SmoothFont.renderSmoothCentered(SmoothFont.getSmoothFont_small_black(), batch, gameAreaWidth / 2, gameAreaHeight / 2 + 100, message);
        }

        if(winningTeam != null){
            SmoothFont.renderSmoothCentered(SmoothFont.getSmoothFont_large_black(), batch, gameAreaWidth / 2 + 200 * winningTeam.getDirection(), gameAreaHeight*0.7f, winningTeam.getName()+" win!");
        }
    }

    public void debugRender(ShapeRenderer renderer) {
        for (Entity ent : entities) {
            ent.debugRender(renderer);
        }
    }

    private void addEntity(Entity ent) {
        entities.add(ent);
    }

    private void addEntities(List<Entity> ents) {
        entities.addAll(ents);
    }

    private void removeEntities(List<Entity> ents) {
        entities.removeAll(ents);
    }

    public void spawnEntity(Entity ent) {
        inbox.add(ent);
    }

    public void killEntity(Entity ent) {
        ent.die(this);
        trash.add(ent);
    }

    //Stuff
    public void createExplosion(float x, float y, float power, float radius) {
        for(Entity ent : entities){
            if(ent.getCenter().dst(x, y) < radius && ent.isSolid()){
                ent.damage(power);
            }
        }
        //visual effect
        //EntityFactory.createExplosionParticles(x, y, radius, this);
        //screen shake
        screenShake.shakeScreen(power * 5);
    }

    public void shakeScreen(float amount){
        screenShake.shakeScreen(amount);
    }

    public void pushBack(Vector2 position, float power, float radius){
        for(Entity ent : entities){
            Vector2 deltaVector = ent.getCenter().sub(position);
            if(deltaVector.len2() < radius*radius && (ent instanceof Ball || ent.isSolid())){
                float forcePercent = (1-deltaVector.len2()/(radius*radius)); //Quadratic decay
                ent.addSpeed(deltaVector.nor().scl(forcePercent * power));
                ent.hookThisEntity(null);
            }
        }
    }

    public void dash(Entity unit, Vector2 direction, float distance, float influenceRadius){
        float stepSize = unit.getImgW()/2;
        float totalDistanceTravelled = 0;
        direction.nor().scl(stepSize);
        Vector2 hitPosition = null;
        while(totalDistanceTravelled < distance){
            if(ball != null && hitPosition == null) {
                if (ball.getCenter().dst2(unit.getCenter()) < influenceRadius*influenceRadius) {
                    hitPosition = new Vector2(unit.getCenter());
                }
            }
            EntityFactory.createTrail(unit.getCenter(), this);
            totalDistanceTravelled += stepSize;
            unit.addPosition(direction);
        }
        if(hitPosition != null){
            ball.addPosition(unit.getCenter().sub(hitPosition));
            ball.setSpeed(direction.scl(0.5f));
            ball.hookThisEntity(null);
        }
    }

    public boolean isUnitSelected(Token token) {
        for(TeamInfo teamInfo : teams){
            if(teamInfo.getSelected().contains(token)){
                return true;
            }
        }
        return false;
    }

    private static boolean rectanglesOverlap(float x1, float y1, float w1, float h1, float x2, float y2, float w2, float h2) {
        return x1 < x2 + w2 && x1 + w1 > x2 && y1 < y2 + h2 && y1 + h1 > y2;
    }

    public void updateCamera() {
        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
        if(cameraFocus != null) {
            camera.translate(cameraFocus.getCenter().x, cameraFocus.getCenter().y);
        }
        camera.translate(screenShake.getScreenDisplacementX(), screenShake.getScreenDisplacementY());
        camera.update();
    }

    public void clearTeams(){
        for(TeamInfo team : teams){
            team.getTokens().clear();
        }
    }

    public void addToTeam(Token entity, Team team){
        for(TeamInfo teamInfo : teams){
            if(teamInfo.getTeam() == team){
                teamInfo.addToTokens(entity);
            }
        }
    }

    public List<Token> getPeopleBelongingTo(Team team){
        for(TeamInfo teamInfo : teams) {
            if (teamInfo.getTeam() == team) {
                return teamInfo.getTokens();
            }
        }
        return new ArrayList<>();
    }

    public List<Token> getPeopleInOtherTeam(Team team){
        for(TeamInfo teamInfo : teams) {
            if (teamInfo.getTeam() != team) {
                return teamInfo.getTokens();
            }
        }
        return new ArrayList<>();
    }

    public void addAI(AI ai){
        if(ai != null) {
            ais.add(ai);
        }
    }

    //Getter & Setter
    public void addFancyStuff(AnimatedParticle p) {
        particles.addParticle(p);
    }

    public ParticleManager getParticles() {
        return particles;
    }

    public List<Entity> getEntities() {
        return entities;
    }

    public OrthographicCamera getCamera() {
        return camera;
    }

    public void setScreenSize(int screenWidth, int screenHeight) {
        camera.viewportWidth = screenWidth;
        camera.viewportHeight = screenHeight;
    }

    public Vector2 toInGameCoordinates(int screenX, int screenY) {
        Vector3 unproject = getCamera().unproject(new Vector3(screenX, screenY, 0));
        return new Vector2(unproject.x, unproject.y);
    }

    public int getGameAreaWidth() {
        return gameAreaWidth;
    }

    public int getGameAreaHeight() {
        return gameAreaHeight;
    }

    public void setCameraFocus(Entity cameraFocus) {
        this.cameraFocus = cameraFocus;
    }

    public Ball getBall() {
        return ball;
    }

    public void setBall(Ball ball) {
        this.ball = ball;
    }

    public Team getWinningTeam() {
        return winningTeam;
    }

    public void setTutorial(boolean tutorial) {
        this.tutorial = tutorial;
    }

    public void setScoreLimit(int scoreLimit) {
        this.scoreLimit = scoreLimit;
    }
}
