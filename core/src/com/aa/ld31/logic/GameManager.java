package com.aa.ld31.logic;

import com.aa.ld31.dal.input.PlayerInputController;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Toni on 02.12.2014.
 */
public class GameManager {
    private int currentLevel;
    private EntityManager entityManager;
    private Map<PlayerInputController, Team> playerToTeamMapping;

    public GameManager(){
        currentLevel = 0;
        playerToTeamMapping = new HashMap<>();
    }

    public void setCurrentLevel(int currentLevel) {
        this.currentLevel = currentLevel;
    }

    public int getCurrentLevel() {
        return currentLevel;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public boolean isSomeonePlayingWithController(){
        for(PlayerInputController playerInputController : playerToTeamMapping.keySet()){
            if(playerInputController.getAssignedController() != null) {
                if(playerToTeamMapping.get(playerInputController) != Team.NONE){
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isPlaying(String player){
        for(PlayerInputController playerInputController : playerToTeamMapping.keySet()){
            if(playerInputController.getName().equals(player)) {
                return playerToTeamMapping.get(playerInputController) != Team.NONE;
            }
        }
        return false;
    }

    public Map<PlayerInputController, Team> getPlayerToTeamMapping() {
        return playerToTeamMapping;
    }

    public void resetAllPlayers() {
        for(PlayerInputController playerInputController : playerToTeamMapping.keySet()){
            playerToTeamMapping.put(playerInputController, Team.NONE);
        }
    }

    public boolean noPlayerAssigned() {
        for(PlayerInputController playerInputController : playerToTeamMapping.keySet()){
            Team team = playerToTeamMapping.get(playerInputController);
            if(team == Team.BLUE || team == Team.ORANGE){
                return false;
            }
        }
        return true;
    }
}
