package com.aa.ld31.logic.level;

import com.aa.ld31.ai.StupidController;
import com.aa.ld31.dal.input.PlayerInputController;
import com.aa.ld31.logic.*;
import com.aa.ld31.logic.objects.Ball;
import com.aa.ld31.logic.objects.Token;

import java.util.Map;

/**
 * Created by Toni on 28.11.2014.
 */
public class LevelLibrary {

    public static final int TRAINING = 1;
    public static final int NORMAL = 3;
    public static final int SCREENSAVER = 999;

    public static final int AI_PASSIVE = 1;
    public static final int AI_NORMAL = 2;
    public static final int AI_AGGRESSIVE = 3;

    public static void createAndInitLevel(GameManager gameManager, int gameMode, EntityFactory.ObjectType forced, int aiType, int unitAmount){
        if(gameMode == TRAINING){
            initTrainingsMatch(gameManager, 3);
        }else if(gameMode == NORMAL){
            initNormalMatch(gameManager, unitAmount, forced, aiType);
        }else if(gameMode == SCREENSAVER){
            initScreenSaver(gameManager);
        }
    }

    private static void initNormalMatch(GameManager gameManager, int amount, EntityFactory.ObjectType forced, int aiType){
        EntityManager entityManager = new EntityManager();

        initPlayerControls(entityManager, gameManager);

        spawnUnits(entityManager, amount, forced);

        addBall(entityManager);
        addAI(entityManager, Team.BLUE, aiType);
        addAI(entityManager, Team.ORANGE, aiType);

        gameManager.setEntityManager(entityManager);
    }

    private static void initTrainingsMatch(GameManager gameManager, int amount){
        EntityManager entityManager = new EntityManager();

        initPlayerControls(entityManager, gameManager);

        spawnUnits(entityManager, amount, null);

        addBall(entityManager);
        addAI(entityManager, Team.BLUE, AI_PASSIVE);
        addAI(entityManager, Team.ORANGE, AI_PASSIVE);

        entityManager.setTutorial(true);

        gameManager.setEntityManager(entityManager);
    }

    private static void initScreenSaver(GameManager gameManager){
        EntityManager entityManager = new EntityManager();

        spawnUnits(entityManager, 8, EntityFactory.ObjectType.BALANCED);

        addBall(entityManager);
        addAI(entityManager, Team.BLUE, AI_AGGRESSIVE);
        addAI(entityManager, Team.ORANGE, AI_AGGRESSIVE);

        entityManager.setScoreLimit(-1);

        gameManager.setEntityManager(entityManager);
    }

    private static void initPlayerControls(EntityManager entityManager, GameManager gameManager){
        Map<PlayerInputController, Team> playerToTeamMapping = gameManager.getPlayerToTeamMapping();
        for(PlayerInputController playerInputController : playerToTeamMapping.keySet()){
            entityManager.addPlayerInputControllerToTeam(playerInputController, playerToTeamMapping.get(playerInputController));
        }
    }

    private static void spawnUnits(EntityManager entityManager, int amount, EntityFactory.ObjectType forced){
        for(int i = 0; i < amount; i++){
            for (int j = 1; j <= 2; j++) {
                Team team = Team.values()[j];
                EntityFactory.ObjectType type = EntityFactory.getRandomToken();
                if(forced != null){
                    if(forced == EntityFactory.ObjectType.BALANCED){
                        type = EntityFactory.getTokenByIndex(i);
                    }else{
                        type = forced;
                    }
                }
                Entity entity = EntityFactory.createEntity(type, team);
                entity.setCenter((float)(Math.random()*entityManager.getGameAreaWidth()/2)+(entityManager.getGameAreaWidth()/2)*(j-1),(float)(Math.random()*entityManager.getGameAreaHeight()));

                entityManager.addToTeam((Token) entity, team);
                entityManager.spawnEntity(entity);
            }
        }
    }

    private static void addBall(EntityManager entityManager){
        Ball ball = (Ball) EntityFactory.createEntity(EntityFactory.ObjectType.BALL, Team.NONE);
        ball.setCenter(entityManager.getGameAreaWidth() / 2, entityManager.getGameAreaHeight() / 2);
        entityManager.spawnEntity(ball);
        entityManager.setBall(ball);
    }

    private static void addAI(EntityManager entityManager, Team team, int difficulty){
        StupidController ai = null;
        if(difficulty == AI_PASSIVE) {
            ai = new StupidController(team, 500, 0.75f, 360, 180, 0.7f);
        }else if(difficulty == AI_NORMAL){
            ai = new StupidController(team, 400, 8f, 50, 500, 0.9f);
        }else if(difficulty == AI_AGGRESSIVE){
            ai = new StupidController(team, 350, 0.88f, 5, 1000, 1.1f);
        }
        entityManager.addAI(ai);
    }
}
