package com.aa.ld31.logic;

/**
 * Created by Toni on 26.04.2014.
 */
public class BasicObject extends Entity {

    private String type;
    protected boolean sideWayBorders = true;

    public BasicObject (String type, Fanta fanta){
        setFanta(fanta, true, true);
        this.type = type;
    }

    @Override
    public void act(EntityManager man) {
        getFanta().act(1);
        computeMovement();
        float extendX = -32;
        if(sideWayBorders){
            if(getBBLeft()< extendX){
                addPosition(extendX-getBBLeft(),0);
                if(getSx()<0){
                    setSx(-getSx());
                }
            }else if(getBBLeft()+getBBWidth() > man.getGameAreaWidth()-extendX){
                addPosition((man.getGameAreaWidth()-extendX)-(getBBLeft()+getBBWidth()),0);
                if(getSx()>0){
                    setSx(-getSx());
                }
            }
        }
        if(getBBBottom()< 0){
            addPosition(0,0-getBBBottom());
            if(getSy()<0){
                setSy(-getSy());
            }
        }else if(getBBBottom()+getBBHeight() > man.getGameAreaHeight()){
            addPosition(0,man.getGameAreaHeight()-(getBBBottom()+getBBHeight()));
            if(getSy()>0){
                setSy(-getSy());
            }
        }
    }

    @Override
    public String getDataType() {
        return type;
    }
}
