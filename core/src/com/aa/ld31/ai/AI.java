package com.aa.ld31.ai;

import com.aa.ld31.logic.EntityManager;

/**
 * Created by Toni on 07.12.2014.
 */
public interface AI {
    void think(EntityManager entityManager);
}
