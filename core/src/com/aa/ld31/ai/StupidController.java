package com.aa.ld31.ai;

import com.aa.ld31.logic.Entity;
import com.aa.ld31.logic.EntityManager;
import com.aa.ld31.logic.Team;
import com.aa.ld31.logic.objects.*;
import com.badlogic.gdx.math.Vector2;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Toni on 07.12.2014.
 */
public class StupidController implements AI{

    private Team team = Team.NONE;

    private float ballOffset = -16;

    private float specialTreshold = 5;
    private float kickDistTreshold = 400;
    private float ballAggression = 1;

    private float separationRadius = 350;
    private float separationForce = 0.88f;
    private Token manOfTheMatch = null;

    private Vector2 centerOfTheField = new Vector2(EntityManager.DEFAULT_VIEWPORT_WIDTH/2, EntityManager.DEFAULT_VIEWPORT_HEIGHT/2);

    private Map<Token, TokenMetaData> tokenMetaDataMap;

    public StupidController(Team team, float separationRadius, float separationForce, float specialTreshold, float kickDistTreshold, float ballAggression) {
        this.team = team;
        this.separationRadius = separationRadius;
        this.separationForce = separationForce;
        this.specialTreshold = specialTreshold;
        this.kickDistTreshold = kickDistTreshold;
        this.ballAggression = ballAggression;

        tokenMetaDataMap = new HashMap<>();
    }

    public void think(EntityManager manager){
        List<Token> peopleBelongingTo = manager.getPeopleBelongingTo(team);
        Ball ball = manager.getBall();
        Vector2 moveDirection;
        float closestDist = Float.MAX_VALUE;
        Token closestToken = null;
        for(Token entity : peopleBelongingTo){
            float totalDistToBall = ball.getCenter().dst(entity.getCenter());
            if(totalDistToBall < closestDist){
                closestDist = totalDistToBall;
                closestToken = entity;
            }
            if(!manager.isUnitSelected(entity)){
                float horizontalDistToBall = team.getDirection()*(ball.getCenter().x-entity.getCenter().x);
                float horizontalDistToGoal = Math.abs((manager.getGameAreaWidth()/2+team.getDirection()*manager.getGameAreaWidth()/2)-entity.getCenter().x);
                moveDirection = ball.getCenter().add(ballOffset*team.getDirection(),(float)(Math.random()*32)-16).sub(entity.getCenter()).nor().scl(ballAggression);
                if(entity != manOfTheMatch) {
                    for (Token otherEntity : peopleBelongingTo) {
                        if (entity != otherEntity) {
                            Vector2 delta = entity.getCenter().sub(otherEntity.getCenter());
                            if (delta.len2() < separationRadius * separationRadius) {
                                moveDirection.mulAdd(delta.nor(), separationForce * (1 - (delta.len() / separationRadius)));
                            }
                        }
                    }
                }
                if(!ball.isAnchored()){
                    if(entity instanceof GrappleToken){
                        moveDirection.add(team.getDirection()*0.35f,0);
                        if(horizontalDistToBall < -200 && horizontalDistToBall > -550){
                            incentiveSpecial(entity, manager);
                        }
                        if(ball.getHookedBy() != null && ball.getHookedBy().getSource() == entity){
                            moveDirection.add(20*team.getDirection(),0);
                        }
                    }else if(entity instanceof ShieldToken){
                        moveDirection.add(centerOfTheField.cpy().sub(entity.getCenter()).scl(0.0005f));
                        if(horizontalDistToBall > 100){
                            incentiveSpecial(entity, manager);
                        }
                    }else if(entity instanceof MagnetToken){
                        if(horizontalDistToBall > 0){
                            incentiveSpecial(entity, manager);
                        }
                    }else if(entity instanceof SpeedToken){
                        if(horizontalDistToBall > 0){
                            incentiveSpecial(entity, manager);
                        }
                    }
                }
                if(totalDistToBall < entity.getImgW()){
                    moveDirection.add(team.getDirection()*ballAggression,0);
                }
                entity.moveInDirection(manager, moveDirection);
            }
        }
        if(ball.isAnchored()){
            manOfTheMatch = null;
        }else {
            manOfTheMatch = closestToken;
        }
    }

    private void incentiveSpecial(Token entity, EntityManager manager){
        TokenMetaData metaData = getMetaData(entity);
        metaData.specialTimer++;
        if(metaData.specialTimer >= specialTreshold) {
            if(entity.isSpecialReady()) {
                entity.tryToUseSpecial(manager, null);
                metaData.specialTimer = 0;
            }
        }
    }

    private TokenMetaData getMetaData(Token token){
        TokenMetaData tokenMetaData = tokenMetaDataMap.get(token);
        if(tokenMetaData == null){
            tokenMetaData = new TokenMetaData();
            tokenMetaDataMap.put(token, tokenMetaData);
        }
        return tokenMetaData;
    }

    private class TokenMetaData{
        private int specialTimer;
    }
}
