package com.aa.ld31.gui;

import com.aa.ld31.gl.SmoothFont;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

/**
 * Created by Toni on 04.12.2014.
 */
public class ShaderLabel extends Label{

    private ShaderProgram shaderProgram;

    public ShaderLabel(CharSequence text, Skin skin) {
        this(text, skin, "default", SmoothFont.getFontShader());
    }

    public ShaderLabel(CharSequence text, Skin skin, String styleName) {
        this(text, skin, styleName, SmoothFont.getFontShader());
    }

    public ShaderLabel(CharSequence text, Skin skin, String styleName, ShaderProgram shaderProgram) {
        super(text, skin, styleName);
        this.shaderProgram = shaderProgram;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.setShader(shaderProgram);
        super.draw(batch, parentAlpha);
        batch.setShader(null);
    }
}
