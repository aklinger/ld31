package com.aa.ld31.gui;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Created by Toni on 08.12.2014.
 */
public class StageWrapper implements InputProcessor {
    private Stage stage;
    private boolean visible;

    public StageWrapper(Stage stage){
        this.stage = stage;

        visible = true;
    }

    public void act(){
        stage.act();
    }

    public void drawStage(){
        if(visible){
            stage.draw();
        }
    }

    public void resize(int screenWidth, int screenHeight){
        stage.getViewport().update(screenWidth, screenHeight);
    }

    public void toggleVisible(){
        this.visible = !this.visible;
    }

    public void setVisible(boolean visible){
        this.visible = visible;
    }

    public boolean isVisible() {
        return visible;
    }

    public Stage getStage() {
        return stage;
    }

    @Override
    public boolean keyDown(int keycode) {
        if(visible){
            stage.keyDown(keycode);
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        if(visible){
            stage.keyUp(keycode);
        }
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        if(visible){
            stage.keyTyped(character);
        }
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if(visible){
            stage.touchDown(screenX, screenY, pointer, button);
        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if(visible){
            stage.touchUp(screenX, screenY, pointer, button);
        }
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if(visible){
            stage.touchDragged(screenX, screenY, pointer);
        }
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        if(visible){
            stage.mouseMoved(screenX, screenY);
        }
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        if(visible){
            stage.scrolled(amount);
        }
        return false;
    }
}
