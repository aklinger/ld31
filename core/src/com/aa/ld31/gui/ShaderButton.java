package com.aa.ld31.gui;

import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.Align;

/**
 * Created by Toni on 07.12.2014.
 */
public class ShaderButton extends Button{

    private final Label label;

    public ShaderButton(String text, Skin skin, String buttonStyle, String labelStyle) {
        super(skin, buttonStyle);
        label = new ShaderLabel(text, skin, labelStyle);
        label.setAlignment(Align.center);
        add(label).expand().fill();
        setSize(getPrefWidth(), getPrefHeight());
    }
}
