package com.aa.ld31.gui;

import com.aa.ld31.gl.SmoothFont;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;

/**
 * Created by Toni on 08.12.2014.
 */
public class ShaderTextField extends TextField{

    public ShaderTextField(String text, Skin skin) {
        super(text, skin);
    }

    public ShaderTextField(String text, Skin skin, String styleName) {
        super(text, skin, styleName);
    }

    @Override
    protected void drawText(Batch batch, BitmapFont font, float x, float y) {
        batch.setShader(SmoothFont.getFontShader());
        super.drawText(batch, font, x, y);
        batch.setShader(null);
    }
}
