package com.aa.ld31;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenManager;
import com.aa.ld31.dal.TextureManager;
import com.aa.ld31.dal.input.ControlSettings;
import com.aa.ld31.dal.input.InputAction;
import com.aa.ld31.dal.input.PlayerInputController;
import com.aa.ld31.gl.SmoothFont;
import com.aa.ld31.gui.ShaderButton;
import com.aa.ld31.gui.ShaderLabel;
import com.aa.ld31.gui.ShaderTextField;
import com.aa.ld31.gui.StageWrapper;
import com.aa.ld31.logic.EntityFactory;
import com.aa.ld31.logic.EntityManager;
import com.aa.ld31.logic.GameManager;
import com.aa.ld31.logic.Team;
import com.aa.ld31.logic.level.LevelLibrary;
import com.aa.ld31.logic.tween.ActorAccessor;
import com.badlogic.gdx.*;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import java.io.IOException;
import java.util.*;
import java.util.List;

public class LD31Game extends ApplicationAdapter {
	private GameManager gameManager;
	private SpriteBatch batch;
	private Skin skin;

	private StageWrapper hudStage;
	private StageWrapper pauseStage;
	private StageWrapper configurePlayersStage;
	private StageWrapper mainMenuStage;
	private StageWrapper endOfGameStage;
	private StageWrapper optionsStage;
	private StageWrapper tutorialStage;
	private StageWrapper creditsStage;

	private Label points1Label;
	private Label points2Label;
	private TextField unitCountTF;
	private ShaderButton prevInstruction;
	private ShaderButton nextInstruction;
	private ShaderButton startGameButton;

	Image controls_explained0;
	Image controls_explained1;
	Image controls_explained2;

	private static TweenManager tweenManager;

	private boolean pause;
	private boolean configurePlayers;
	private boolean options;
	private boolean credits;

	private int currentMode = LevelLibrary.TRAINING;

	private int aiType = LevelLibrary.AI_NORMAL;
	private int unitCount = 4;
	private EntityFactory.ObjectType forced = EntityFactory.ObjectType.BALANCED;

	@Override
	public void create () {
		try {
			initRenderStuff();
			initStage();
			initInput();
			initTween();
			initGameStuff();

			loadLevel(LevelLibrary.SCREENSAVER);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void initRenderStuff() throws IOException {
		batch = new SpriteBatch();
		TextureManager.getInstance().loadTextures("gfx/texture_atlas.atlas");
		TextureManager.getInstance().loadTextureMetaData("textures.res");

		skin = new Skin();
		SmoothFont.init();
		SmoothFont.addFontToSkin(skin);
		skin.addRegions(new TextureAtlas(Gdx.files.internal("gfx/gui/myskin.atlas")));
		skin.load(Gdx.files.internal("gfx/gui/myskin.json"));
	}

	private void initStage() {
		//HUD
		Stage hudStage = new Stage(new StretchViewport(EntityManager.DEFAULT_VIEWPORT_WIDTH, EntityManager.DEFAULT_VIEWPORT_HEIGHT), batch);

		points1Label = new ShaderLabel("",skin,"smooth_large");
		points1Label.setPosition(30, 55);
		hudStage.addActor(points1Label);

		points2Label = new ShaderLabel("",skin,"smooth_large");
		points2Label.setWidth(300);
		points2Label.setAlignment(Align.right);
		points2Label.setPosition(EntityManager.DEFAULT_VIEWPORT_WIDTH-300-30,55);
		hudStage.addActor(points2Label);

		ShaderLabel name1Label = new ShaderLabel(Team.ORANGE.getName(), skin, "smooth_small");
		name1Label.setPosition(20, 0);
		hudStage.addActor(name1Label);

		ShaderLabel name2Label = new ShaderLabel(Team.BLUE.getName(), skin, "smooth_small");
		name2Label.setPosition(EntityManager.DEFAULT_VIEWPORT_WIDTH-230, 0);
		hudStage.addActor(name2Label);

		this.hudStage = new StageWrapper(hudStage);
		this.hudStage.setVisible(true);

		//PAUSE
		Stage pauseStage = new Stage(new StretchViewport(EntityManager.DEFAULT_VIEWPORT_WIDTH, EntityManager.DEFAULT_VIEWPORT_HEIGHT), batch);


		ShaderButton mainMenuButton = new ShaderButton("Exit to Main Menu", skin, "default", "smooth_large");
		mainMenuButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				gotoMainMenu();
			}
		});

		ShaderButton restartButton = new ShaderButton("Restart", skin, "default", "smooth_large");
		restartButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				configurePlayers = true;
				pause = false;
				gameManager.resetAllPlayers();
			}
		});

		ShaderButton resumeButton = new ShaderButton("Resume", skin, "default", "smooth_large");
		resumeButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				pause = !pause;
			}
		});

		Table table = new Table(skin);
		table.setFillParent(true);
		table.add(new ShaderLabel("PAUSED", skin, "infobox")).space(10);
		table.row();
		table.add(resumeButton).space(10);
		table.row();
		table.add(restartButton).space(10);
		table.row();
		table.add(mainMenuButton).space(30);

		pauseStage.addActor(table);

		this.pauseStage = new StageWrapper(pauseStage);
		this.pauseStage.setVisible(false);

		//OPTIONS STAGE
		Stage optionsStage = new Stage(new StretchViewport(EntityManager.DEFAULT_VIEWPORT_WIDTH, EntityManager.DEFAULT_VIEWPORT_HEIGHT), batch);

		int imgX = 80;
		int imgY = 10;
		int iconSize = 32;
		String onlyText = "Only    ";

		Image grapperIcon = new Image(new TextureRegion(TextureManager.getInstance().getTexture("grapple")));
		grapperIcon.setPosition(imgX,imgY);
		grapperIcon.setSize(iconSize,iconSize);
		Image magnetIcon = new Image(new TextureRegion(TextureManager.getInstance().getTexture("magnet")));
		magnetIcon.setPosition(imgX,imgY);
		magnetIcon.setSize(iconSize,iconSize);
		Image shieldIcon = new Image(new TextureRegion(TextureManager.getInstance().getTexture("shield")));
		shieldIcon.setPosition(imgX,imgY);
		shieldIcon.setSize(iconSize,iconSize);
		Image runnerIcon = new Image(new TextureRegion(TextureManager.getInstance().getTexture("runner")));
		runnerIcon.setPosition(imgX,imgY);
		runnerIcon.setSize(iconSize,iconSize);

		ShaderButton onlyGrapperButton = new ShaderButton(onlyText, skin, "toggle", "smooth_small");
		onlyGrapperButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				forced = EntityFactory.ObjectType.GRAPPLE;
			}
		});
		onlyGrapperButton.addActor(grapperIcon);

		ShaderButton onlyMagnetButton = new ShaderButton(onlyText, skin, "toggle", "smooth_small");
		onlyMagnetButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				forced = EntityFactory.ObjectType.MAGNET;
			}
		});
		onlyMagnetButton.addActor(magnetIcon);

		ShaderButton onlyRunnerButton = new ShaderButton(onlyText, skin, "toggle", "smooth_small");
		onlyRunnerButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				forced = EntityFactory.ObjectType.RUNNER;
			}
		});
		onlyRunnerButton.addActor(runnerIcon);

		ShaderButton onlyShieldButton = new ShaderButton(onlyText, skin, "toggle", "smooth_small");
		onlyShieldButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				forced = EntityFactory.ObjectType.SHIELD;
			}
		});
		onlyShieldButton.addActor(shieldIcon);

		ShaderButton allRandomButton = new ShaderButton("Random", skin, "toggle", "smooth_small");
		allRandomButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				forced = null;
			}
		});
		ShaderButton balancedButton = new ShaderButton("Balanced", skin, "toggle", "smooth_small");
		balancedButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				forced = EntityFactory.ObjectType.BALANCED;
			}
		});
		ButtonGroup unitTypeButtonGroup = new ButtonGroup(onlyGrapperButton, onlyMagnetButton, onlyRunnerButton, onlyShieldButton, allRandomButton, balancedButton);
		unitTypeButtonGroup.setUncheckLast(true);
		unitTypeButtonGroup.setMinCheckCount(1);
		unitTypeButtonGroup.setMaxCheckCount(1);
		balancedButton.setChecked(true);

		float space = 5;

		final Table unitTypeTable = new Table(skin);
		unitTypeTable.setCenterPosition(-200, EntityManager.DEFAULT_VIEWPORT_HEIGHT / 4);
		unitTypeTable.add(new ShaderLabel("Unit Types", skin, "infobox")).space(10).colspan(3);
		unitTypeTable.row();
		unitTypeTable.add(onlyGrapperButton).size(120, 50).space(space);
		unitTypeTable.add(onlyRunnerButton).size(120, 50).space(space);
		unitTypeTable.add(onlyShieldButton).size(120, 50).space(space);
		unitTypeTable.row();
		unitTypeTable.add(onlyMagnetButton).size(120, 50).space(space);
		unitTypeTable.add(allRandomButton).size(120, 50).space(space);
		unitTypeTable.add(balancedButton).size(120, 50).space(space);
		unitTypeTable.row();

		optionsStage.addActor(unitTypeTable);


		ShaderButton passiveAIButton = new ShaderButton("Easy", skin, "toggle", "smooth_small");
		passiveAIButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				aiType = LevelLibrary.AI_PASSIVE;
			}
		});
		ShaderButton normalAIButton = new ShaderButton("Normal", skin, "toggle", "smooth_small");
		normalAIButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				aiType = LevelLibrary.AI_NORMAL;
			}
		});
		ShaderButton aggressiveAIButton = new ShaderButton("Hard", skin, "toggle", "smooth_small");
		aggressiveAIButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				aiType = LevelLibrary.AI_AGGRESSIVE;
			}
		});

		unitCountTF = new ShaderTextField(""+unitCount, skin);
		unitCountTF.setWidth(100);

		ButtonGroup aiDifficultyButtonGroup = new ButtonGroup(passiveAIButton, normalAIButton, aggressiveAIButton);
		aiDifficultyButtonGroup.setUncheckLast(true);
		aiDifficultyButtonGroup.setMinCheckCount(1);
		aiDifficultyButtonGroup.setMaxCheckCount(1);
		normalAIButton.setChecked(true);

		final Table aiDifficultyTable = new Table(skin);
		aiDifficultyTable.setCenterPosition(+1500, EntityManager.DEFAULT_VIEWPORT_HEIGHT / 4);
		aiDifficultyTable.add(new ShaderLabel("AI Difficulty", skin, "infobox")).space(10).colspan(3);
		aiDifficultyTable.row();
		aiDifficultyTable.add(passiveAIButton).size(110, 50).space(space);
		aiDifficultyTable.add(normalAIButton).size(110, 50).space(space);
		aiDifficultyTable.add(aggressiveAIButton).size(110, 50).space(space);
		aiDifficultyTable.row();
		aiDifficultyTable.add(new ShaderLabel("Units per Team:", skin, "infobox")).size(220, 50).colspan(2).space(space).spaceTop(25);
		aiDifficultyTable.add(unitCountTF).size(110, 50).space(space).spaceTop(25);
		aiDifficultyTable.row();

		optionsStage.addActor(aiDifficultyTable);

		this.optionsStage = new StageWrapper(optionsStage);
		this.optionsStage.setVisible(true);

		//Credits STAGE
		Stage creditsStage = new Stage(new StretchViewport(EntityManager.DEFAULT_VIEWPORT_WIDTH, EntityManager.DEFAULT_VIEWPORT_HEIGHT), batch);

		final ShaderButton creditsBackground = new ShaderButton("Created by Anton Klinger\n" +
				"Testers: Vinzenz Brodschneider, Mathias Sgarz\n\n" +
				"Conceived during Ludum Dare 31 (v1.1)", skin, "default", "smooth_small");
		creditsBackground.setCenterPosition(EntityManager.DEFAULT_VIEWPORT_WIDTH/2, EntityManager.DEFAULT_VIEWPORT_HEIGHT + 200);
		creditsBackground.setDisabled(true);

		creditsStage.addActor(creditsBackground);

		this.creditsStage = new StageWrapper(creditsStage);
		this.creditsStage.setVisible(true);

		//CONFIGURE PLAYER CONTROLS
		Stage configurePlayersStage = new Stage(new StretchViewport(EntityManager.DEFAULT_VIEWPORT_WIDTH, EntityManager.DEFAULT_VIEWPORT_HEIGHT), batch);

		startGameButton = new ShaderButton("Start Match", skin, "default", "smooth_large");
		startGameButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				pause = false;
				configurePlayers = false;
				restart();
			}
		});
		startGameButton.setCenterPosition(EntityManager.DEFAULT_VIEWPORT_WIDTH / 2, 70);

		ShaderButton moreBackground = new ShaderButton("", skin, "background", "smooth_large");
		moreBackground.setDisabled(true);
		moreBackground.setSize(700,450);
		moreBackground.setCenterPosition(EntityManager.DEFAULT_VIEWPORT_WIDTH/2, EntityManager.DEFAULT_VIEWPORT_HEIGHT/2);

		ShaderLabel controllerInfo = new ShaderLabel("Playing with gamepads and friends recommended.", skin, "smooth_small");
		controllerInfo.setCenterPosition(EntityManager.DEFAULT_VIEWPORT_WIDTH / 2, 170);

		configurePlayersStage.addActor(startGameButton);
		configurePlayersStage.addActor(moreBackground);
		configurePlayersStage.addActor(controllerInfo);
		this.configurePlayersStage = new StageWrapper(configurePlayersStage);
		this.configurePlayersStage.setVisible(false);

		//TUTORIAL
		Stage tutorialStage = new Stage(new StretchViewport(EntityManager.DEFAULT_VIEWPORT_WIDTH, EntityManager.DEFAULT_VIEWPORT_HEIGHT), batch);

		controls_explained0 = new Image(skin.getDrawable("controls_explained0"));
		controls_explained1 = new Image(skin.getDrawable("controls_explained1"));
		controls_explained2 = new Image(skin.getDrawable("controls_explained2"));

		controls_explained0.setCenterPosition(EntityManager.DEFAULT_VIEWPORT_WIDTH/2-20-450, EntityManager.DEFAULT_VIEWPORT_HEIGHT-200);
		controls_explained1.setCenterPosition(EntityManager.DEFAULT_VIEWPORT_WIDTH/2-20, EntityManager.DEFAULT_VIEWPORT_HEIGHT-200);
		controls_explained2.setCenterPosition(EntityManager.DEFAULT_VIEWPORT_WIDTH/2-20+450, EntityManager.DEFAULT_VIEWPORT_HEIGHT-200);

		controls_explained0.setScale(3);
		controls_explained1.setScale(3);
		controls_explained2.setScale(3);

		tutorialStage.addActor(controls_explained0);
		tutorialStage.addActor(controls_explained1);
		tutorialStage.addActor(controls_explained2);

		prevInstruction = new ShaderButton("    ", skin, "arrow_left", "smooth_small");
		prevInstruction.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				gameManager.getEntityManager().prevTutorial();
			}
		});
		prevInstruction.setCenterPosition(EntityManager.DEFAULT_VIEWPORT_WIDTH * 0.15f, 200);

		nextInstruction = new ShaderButton("    ", skin, "arrow_right", "smooth_small");
		nextInstruction.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				gameManager.getEntityManager().nextTutorial();
			}
		});
		nextInstruction.setCenterPosition(EntityManager.DEFAULT_VIEWPORT_WIDTH * 0.85f, 200);

		tutorialStage.addActor(prevInstruction);
		tutorialStage.addActor(nextInstruction);

		this.tutorialStage = new StageWrapper(tutorialStage);
		this.tutorialStage.setVisible(false);

		//MAIN MENU
		Stage mainMenuStage = new Stage(new StretchViewport(EntityManager.DEFAULT_VIEWPORT_WIDTH, EntityManager.DEFAULT_VIEWPORT_HEIGHT), batch);

		ShaderButton startTrainingButton = new ShaderButton("Start Training", skin, "default", "smooth_large");
		startTrainingButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				startMatch(LevelLibrary.TRAINING);
			}
		});

		ShaderButton startSeriousMatchButton = new ShaderButton("Start a Match", skin, "default", "smooth_large");
		startSeriousMatchButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				startMatch(LevelLibrary.NORMAL);
			}
		});

		ShaderButton optionsButton = new ShaderButton("Options", skin, "default", "smooth_large");
		optionsButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				TweenCallback toggleOptions = new TweenCallback() {
					@Override
					public void onEvent(int type, BaseTween<?> source) {
						if(type == COMPLETE){
							options = false;
						}
					}
				};
				if(options){
					Tween.to(unitTypeTable, ActorAccessor.POSITION_XY, 30f).target(-200,EntityManager.DEFAULT_VIEWPORT_HEIGHT / 4).setCallback(toggleOptions).start(tweenManager);
					Tween.to(aiDifficultyTable, ActorAccessor.POSITION_XY, 30f).target(1500,EntityManager.DEFAULT_VIEWPORT_HEIGHT / 4).setCallback(toggleOptions).start(tweenManager);
				}else{
					options = true;
					Tween.to(unitTypeTable, ActorAccessor.POSITION_XY, 30f).target(200, EntityManager.DEFAULT_VIEWPORT_HEIGHT / 4).start(tweenManager);
					Tween.to(aiDifficultyTable, ActorAccessor.POSITION_XY, 30f).target(EntityManager.DEFAULT_VIEWPORT_WIDTH-200, EntityManager.DEFAULT_VIEWPORT_HEIGHT / 4).start(tweenManager);
				}
			}
		});

		ShaderButton creditsButton = new ShaderButton("Credits", skin, "default", "smooth_large");
		creditsButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				TweenCallback toggleCredits = new TweenCallback() {
					@Override
					public void onEvent(int type, BaseTween<?> source) {
						if(type == COMPLETE){
							credits = false;
						}
					}
				};
				if(credits){
					Tween.to(creditsBackground, ActorAccessor.POSITION_XY, 30f).target(EntityManager.DEFAULT_VIEWPORT_WIDTH/2-creditsBackground.getWidth()/2,EntityManager.DEFAULT_VIEWPORT_HEIGHT + creditsBackground.getHeight()+10).setCallback(toggleCredits).start(tweenManager);
				}else{
					credits = true;
					Tween.to(creditsBackground, ActorAccessor.POSITION_XY, 30f).target(EntityManager.DEFAULT_VIEWPORT_WIDTH/2-creditsBackground.getWidth()/2, EntityManager.DEFAULT_VIEWPORT_HEIGHT - creditsBackground.getHeight()-10).start(tweenManager);
				}
			}
		});

		Image title = new Image(skin.getDrawable("title1"));

		Table mainMenuTable = new Table(skin);
		mainMenuTable.setFillParent(true);

		//mainMenuTable.add(new ShaderLabel("Hyper Soccer - Main Menu", skin, "title")).space(20);
		mainMenuTable.add(title).size(64 * 5, 32 * 5).space(10);
		mainMenuTable.row();
		mainMenuTable.add(startTrainingButton).space(15);
		mainMenuTable.row();
		mainMenuTable.add(startSeriousMatchButton).space(15);
		mainMenuTable.row();
		mainMenuTable.add(optionsButton).space(15);
		mainMenuTable.row();
		mainMenuTable.add(creditsButton).space(15);
		mainMenuTable.row();

		mainMenuStage.addActor(mainMenuTable);

		this.mainMenuStage = new StageWrapper(mainMenuStage);

		//END OF GAME STAGE
		Stage endOfGameStage = new Stage(new StretchViewport(EntityManager.DEFAULT_VIEWPORT_WIDTH, EntityManager.DEFAULT_VIEWPORT_HEIGHT), batch);

		ShaderButton exitToMainMenuButton = new ShaderButton("Back to Menu", skin, "default", "smooth_large");
		exitToMainMenuButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				gotoMainMenu();
			}
		});
		exitToMainMenuButton.setCenterPosition(EntityManager.DEFAULT_VIEWPORT_WIDTH / 2, 80);

		endOfGameStage.addActor(exitToMainMenuButton);
		this.endOfGameStage = new StageWrapper(endOfGameStage);
		this.endOfGameStage.setVisible(false);
	}

	private void startMatch(int mode){
		currentMode = mode;
		configurePlayers = true;
		gameManager.resetAllPlayers();
		mainMenuStage.setVisible(false);
		//restart(LevelLibrary.SCREENSAVER);
	}
	private void gotoMainMenu(){
		mainMenuStage.setVisible(true);
		configurePlayers = false;
		pause = false;
		credits = false;
		restart(LevelLibrary.SCREENSAVER);
	}

	private void initInput(){
		InputMultiplexer inputMultiplexer = new InputMultiplexer();
		inputMultiplexer.addProcessor(ControlSettings.getInstance());
		inputMultiplexer.addProcessor(hudStage);
		inputMultiplexer.addProcessor(pauseStage);
		inputMultiplexer.addProcessor(configurePlayersStage);
		inputMultiplexer.addProcessor(mainMenuStage);
		inputMultiplexer.addProcessor(endOfGameStage);
		inputMultiplexer.addProcessor(optionsStage);
		inputMultiplexer.addProcessor(tutorialStage);
		Gdx.input.setInputProcessor(inputMultiplexer);
		Controllers.addListener(ControlSettings.getInstance());
	}

	private void initTween(){
		tweenManager = new TweenManager();
		Tween.registerAccessor(Actor.class, new ActorAccessor());
	}

	private void initGameStuff(){
		gameManager = new GameManager();
		Map<PlayerInputController, Team> playerToTeamMapping = gameManager.getPlayerToTeamMapping();
		List<PlayerInputController> playerInputControllers = ControlSettings.getInstance().getPlayerInputControllers();
		for(PlayerInputController playerInputController : playerInputControllers){
			playerToTeamMapping.put(playerInputController, Team.NONE);
		}
	}

	private int tryToParseUnitAmount(){
		String text = unitCountTF.getText();
		try{
			unitCount = Integer.parseInt(text);
		}catch (NumberFormatException ex){
			unitCount = 4;
		}
		if(unitCount > 1000){
			unitCount = 1000;
		}else if(unitCount < 0){
			unitCount = 0;
		}
		unitCountTF.setText(""+unitCount);
		return unitCount;
	}

	private void loadLevel(int mode){
		LevelLibrary.createAndInitLevel(gameManager, mode, forced, aiType, tryToParseUnitAmount());
		gameManager.getEntityManager().initSomeStuff();
	}

	private void restart(){
		restart(currentMode);
	}

	private void restart(int mode){
		loadLevel(mode);
	}

	@Override
	public void render () {

		update();

		Gdx.gl.glClearColor(0.75f, 0.85f, 0.75f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		renderGameWorld(batch);

		renderHud();

		endOfGameStage.drawStage();

		if(configurePlayers || pause || mainMenuStage.isVisible()) {
			fadeBlack(batch, 0.75f);
		}

		renderTheMenues();

		renderThatConfigurationThingy(batch);
	}

	private void update(){
		if(Gdx.input.isKeyPressed(Input.Keys.ESCAPE)){
			if(configurePlayers){
				gotoMainMenu();
			}else if(!mainMenuStage.isVisible()){
				pause = true;
			}
		}
		if(ControlSettings.getInstance().isPressed(InputAction.PAUSE, 0)){
			pause = !pause;
		}
		if(configurePlayers && !mainMenuStage.isVisible()){
			checkSelectedTeam();
			if(Gdx.input.isKeyPressed(Input.Keys.ENTER)){
				configurePlayers = false;
				restart();
			}
		}

		hudStage.act();
		pauseStage.act();
		configurePlayersStage.act();
		mainMenuStage.act();
		endOfGameStage.act();
		optionsStage.act();
		tutorialStage.act();
		creditsStage.act();

		updateHud(gameManager);

		pauseStage.setVisible(pause && !configurePlayers);
		configurePlayersStage.setVisible(configurePlayers && !mainMenuStage.isVisible());
		endOfGameStage.setVisible( (gameManager.getEntityManager().getWinningTeam() != null || gameManager.getEntityManager().isOnLastPage()) && !pause && !configurePlayers);
		optionsStage.setVisible(options && mainMenuStage.isVisible());
		creditsStage.setVisible(credits && mainMenuStage.isVisible());
		tutorialStage.setVisible(currentMode == LevelLibrary.TRAINING && !configurePlayers && !pause && !mainMenuStage.isVisible());

		nextInstruction.setDisabled(gameManager.getEntityManager().isOnLastPage());
		prevInstruction.setDisabled(gameManager.getEntityManager().isOnFirstPage());

		if (currentMode == LevelLibrary.TRAINING) {
			controls_explained0.setVisible(gameManager.isPlaying("P1"));
			controls_explained1.setVisible(gameManager.isPlaying("P2"));
			controls_explained2.setVisible(gameManager.isSomeonePlayingWithController());
		}

		if(!pause) {
			tweenManager.update(1);
			gameManager.getEntityManager().act();
		}
		ControlSettings.getInstance().actFrame();
	}

	private void checkSelectedTeam(){
		Map<PlayerInputController, Team> playerToTeamMapping = gameManager.getPlayerToTeamMapping();
		for(PlayerInputController playerInputController : playerToTeamMapping.keySet()){
			Team team = playerToTeamMapping.get(playerInputController);
			if(playerInputController.isPressed(InputAction.ACTION_MOVE_LEFT)){
				if(team == Team.BLUE){
					team = Team.NONE;
				}else if(team == Team.NONE){
					team = Team.ORANGE;
				}
			}else if(playerInputController.isPressed(InputAction.ACTION_MOVE_RIGHT)){
				if(team == Team.ORANGE){
					team = Team.NONE;
				}else if(team == Team.NONE){
					team = Team.BLUE;
				}
			}
			playerToTeamMapping.put(playerInputController, team);
		}
	}

	private void updateHud(GameManager gameManager){
		points1Label.setText(""+gameManager.getEntityManager().getReasonableScore(Team.ORANGE));
		points2Label.setText(""+gameManager.getEntityManager().getReasonableScore(Team.BLUE));
	}

	private void renderGameWorld(SpriteBatch batch){
		batch.setProjectionMatrix(gameManager.getEntityManager().getCamera().combined);
		batch.begin();
		gameManager.getEntityManager().render(batch);
		batch.end();
	}

	private void fadeBlack(SpriteBatch batch, float alpha){
		TextureRegion black = TextureManager.getInstance().getTexture("block");
		batch.begin();
		batch.setColor(0,0,0,alpha);
		batch.draw(black,0,0,EntityManager.DEFAULT_VIEWPORT_WIDTH, EntityManager.DEFAULT_VIEWPORT_HEIGHT);
		batch.end();
	}

	private void renderHud(){
		hudStage.drawStage();
	}

	private void renderTheMenues(){
		tutorialStage.drawStage();
		if(mainMenuStage.isVisible()) {
			mainMenuStage.drawStage();
			optionsStage.drawStage();
		}else if(pause && !configurePlayers){
			pauseStage.drawStage();
		}
		creditsStage.drawStage();
	}

	private void renderThatConfigurationThingy(SpriteBatch batch){

		if(configurePlayers) {
			if (gameManager.noPlayerAssigned()) {
				startGameButton.setVisible(false);
			} else {
				startGameButton.setVisible(true);
			}
		}
		configurePlayersStage.drawStage();

		if(configurePlayers) {
			float ypos = EntityManager.DEFAULT_VIEWPORT_HEIGHT - 210;
			float yDiff = -80;
			float xpos = EntityManager.DEFAULT_VIEWPORT_WIDTH / 2 - 20;
			float xDiff = -270;

			Map<PlayerInputController, Team> playerToTeamMapping = gameManager.getPlayerToTeamMapping();
			List<PlayerInputController> playerList = new ArrayList<>(playerToTeamMapping.keySet());
			Collections.sort(playerList);

			batch.begin();
			batch.setColor(1, 1, 1, 1);
			for (PlayerInputController playerInputController : playerList) {
				TextureRegion controlsTextureRegion = TextureManager.getInstance().getTexture(playerInputController.getTextureName());
				Team team = playerToTeamMapping.get(playerInputController);
				float drawX = xpos + xDiff * team.getDirection();
				float drawY = ypos;

				batch.draw(controlsTextureRegion, drawX, drawY, 64, 64);
				SmoothFont.renderSmooth(SmoothFont.getSmoothFont_small_white(), batch, drawX-20, drawY+30, playerInputController.getName());

				ypos += yDiff;

				if(team != Team.ORANGE) {
					Drawable arrow_left = skin.getDrawable("arrow0");
					arrow_left.draw(batch, drawX - 60, drawY + 10, 32, 32);
				}
				if(team != Team.BLUE) {
					Drawable arrow_right = skin.getDrawable("arrow3");
					arrow_right.draw(batch, drawX + 75, drawY + 10, 32, 32);
				}
			}
			SmoothFont.renderSmoothCentered(SmoothFont.getSmoothFont_large_white(), batch, EntityManager.DEFAULT_VIEWPORT_WIDTH / 2, EntityManager.DEFAULT_VIEWPORT_HEIGHT + 15, "Press Left or Right to select your Team");
			SmoothFont.renderSmooth(SmoothFont.getSmoothFont_small_white(),batch,EntityManager.DEFAULT_VIEWPORT_WIDTH/2-100-220, EntityManager.DEFAULT_VIEWPORT_HEIGHT-85,Team.ORANGE.getName());
			SmoothFont.renderSmooth(SmoothFont.getSmoothFont_small_white(),batch,EntityManager.DEFAULT_VIEWPORT_WIDTH/2-100+240, EntityManager.DEFAULT_VIEWPORT_HEIGHT-85,Team.BLUE.getName());
			batch.end();
		}
	}

	public static TweenManager getTweenManager() {
		return tweenManager;
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		hudStage.resize(width, height);
		pauseStage.resize(width, height);
		configurePlayersStage.resize(width, height);
		mainMenuStage.resize(width, height);
		endOfGameStage.resize(width, height);
		optionsStage.resize(width, height);
		tutorialStage.resize(width, height);
		creditsStage.resize(width, height);
	}
}
