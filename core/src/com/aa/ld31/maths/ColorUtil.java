package com.aa.ld31.maths;

import com.badlogic.gdx.graphics.Color;

/**
 * Created by Toni on 24.08.2014.
 */
public class ColorUtil {

    public static Color randomColor(float alpha){
        return new Color((float)(Math.random()),(float)(Math.random()),(float)(Math.random()),alpha);
    }

    public static Color jumbleColor(Color source, float displacement){
        return new Color((float)Calc.fastRange((Math.random()*displacement*2-displacement)+source.r,0,1),(float)Calc.fastRange((Math.random()*displacement*2-displacement)+source.g,0,1),(float)Calc.fastRange((Math.random()*displacement*2-displacement)+source.b,0,1),source.a);
    }

}
