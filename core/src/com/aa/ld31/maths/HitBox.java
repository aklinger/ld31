package com.aa.ld31.maths;

import com.aa.ld31.logic.Entity;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * An interface to mark Classes as having Hitboxes.
 * <p/>
 * @author Toni
 */
public interface HitBox {
    //Konstanten
    int AABB = 1;
    int CIRCLE = 2;
    //Abstrakte Methoden
    Entity getParent();
    Rectangle getBoundingBox();
    Vector2 getCenter();
    int getType();
    float getRadius();
}
